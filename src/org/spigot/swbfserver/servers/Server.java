package org.spigot.swbfserver.servers;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.spigot.swbfserver.GUI.ServerPanel.serverStatusUpdateHandlerWrapper;
import org.spigot.swbfserver.exceptions.InvalidMapException;
import org.spigot.swbfserver.maps.Map;
import org.spigot.swbfserver.master.MasterServerUpdateThread;

public class Server implements Serializable {

	private static final long serialVersionUID = 8433543563713511492L;

	/*
	 * private java.util.Map<String, ServerMap> mapsRight = new LinkedHashMap<>(); private java.util.Map<String, ServerMap> mapsLeft = new TreeMap<>();
	 */
	private List<ServerMap> mapsLeft = new ArrayList<>();
	private List<ServerMap> mapsRight = new ArrayList<>();

	private Process p = null;

	private ServerSettings settings;

	public Server(ServerSettings set) {
		this.settings = set;
		validate();
		detectMaps();
	}

	public ServerSettings getSettings() {
		return settings;
	}

	private int ti = -1;

	private List<ServerMap> listToMapList(List<Map> m) {
		List<ServerMap> cm = new ArrayList<>();
		for (Map s : m) {
			ServerMap d = new ServerMap(s);
			d.setFolder(s.getPath());
			cm.add(d);
		}
		return cm;
	}

	private void detectMaps() {
		settings.validate();
		mapsLeft = listToMapList(settings.getMapCodesLeft());
		mapsRight = listToMapList(settings.getMapCodesRight());
		settings.unvalidate();
		resortMaps();
	}

	public void setTabIndex(int index) {
		this.ti = index;
	}

	public int getTabIndex() {
		return ti;
	}

	private serverStatusUpdateHandlerWrapper handler;

	private boolean allowRestart;

	protected boolean allowStop;

	public void setUpdateHandler(serverStatusUpdateHandlerWrapper handler) {
		this.handler = handler;
		settings.setUpdateHandler(handler.getHandler());
	}

	public void start() {
		if (!processRunning()) {
			if (!mapsRight.isEmpty()) {
				try {
					String[] cmd = getCommand();
					p = Runtime.getRuntime().exec(cmd, null, new File(ServerManager.getServerFolderPath()));
					settings.updateStatus("Starting server...");
					allowRestart = true;
					allowStop = true;
					new ServerThread(new Runnable() {

						@Override
						public void run() {
							Process p = Server.this.p;
							try {
								handler.setServerStarted();
								settings.updateStatus("Server running");
								MasterServerUpdateThread.addServer(Server.this);
								p.waitFor();
								settings.updateStatus("Server exited");
							} catch (InterruptedException e) {
							}
							if (settings.hasAutoRestartEnabled() && allowRestart) {
								restart();
							} else if (allowStop) {
								stop();
							}
						}

					});
				} catch (IOException e) {
					e.printStackTrace();
					settings.updateStatus("Failed to start server");
				}
			}
		}
	}

	private boolean processRunning() {
		if (p == null) {
			return false;
		} else {
			try {
				p.exitValue();
				p = null;
				return false;
			} catch (IllegalThreadStateException e) {
				return true;
			}
		}
	}

	private String getExecPath() {
		return ServerManager.getServerFolderPath() + "/" + "battlefront.exe";
	}

	private String[] getCommand() {
		List<String> cmds = new ArrayList<>();
		String sa = ServerManager.getPreArguments();
		if (!sa.isEmpty()) {
			cmds.add(sa);
		}
		cmds.add(getExecPath());
		cmds.add("/win");

		cmds.add("/norender");

		cmds.add("/autonet");
		cmds.add("dedicated");

		cmds.add("/resolution");
		cmds.add("320");
		cmds.add("240");

		cmds.add("/nosound");

		cmds.add("/throttle");
		cmds.add("3072");

		cmds.add("/gamename");
		cmds.add(settings.getServerName());

		cmds.add("/playerlimit");
		cmds.add(settings.getMaxPlayers() + "");

		cmds.add("/playercount");
		cmds.add(settings.getMinPlayers() + "");

		cmds.add("/bots");
		cmds.add(settings.getAiPerTeam() + "");

		if (!settings.isTeamAutoSelect()) {
			cmds.add("/sideselect");
		}

		if (!settings.showPlayerNames()) {
			cmds.add("/nonames");
		}

		if (!settings.hasTeamDamage()) {
			cmds.add("/noteamdamage");
		}

		if (settings.hasPassword()) {
			String pw = settings.getPassword();
			if (pw != null) {
				if (!pw.isEmpty()) {
					cmds.add("/password");
					cmds.add(pw);
				}
			}
		}

		cmds.add("/difficulty");
		cmds.add(settings.getAiDifficulty() + "");

		cmds.add("/spawn");
		cmds.add(settings.getSpawnInvincibility() + "");

		if (settings.hasHeroes()) {
			cmds.add("/heroes");
		}

		if (settings.hasRandomMapOrder()) {
			cmds.add("/randomize");
		}

		for (ServerMap m : mapsRight) {
			cmds.add(m.getMapCode().toLowerCase());
			cmds.add(m.getReinforcements1() + "");
			cmds.add(m.getReinforcements2() + "");
		}
		int len = cmds.size();
		String[] q = new String[len];
		int i = 0;
		for (String s : cmds) {
			q[i] = s;
			i++;
		}
		return q;
	}

	public static void dump(List<String> d) {
		StringBuilder sb = new StringBuilder("DUMP: ");
		for (String s : d) {
			sb.append(s + " ");
		}
		System.out.println(sb.toString());
	}

	private void stopWait() {
		allowRestart = false;
		settings.updateStatus("Stopping server...");
		if (processRunning()) {
			try {
				p.destroy();
			} catch (Throwable e) {
			}
		}
	}

	public void stop() {
		stopWait();
		if (handler != null) {
			handler.setServerStopped();
		}
		settings.updateStatus("Server not running");
		MasterServerUpdateThread.removeServer(this);
	}

	public void restart() {
		new Thread() {
			@Override
			public void run() {
				settings.updateStatus("Server restart thread");
				allowStop = false;
				Server.this.stopWait();
				synchronized (this) {
					try {
						this.wait(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				Server.this.start();
			}
		}.start();
	}

	public void removeMap(String code) {
		ServerMap f = null;
		for (ServerMap m : mapsRight) {
			if (m.toString().equals(code)) {
				f = m;
			}
		}
		if (f != null) {
			mapsRight.remove(f);
			mapsLeft.add(f);
			settings.setCurrentStatus();
			resortMaps();
		}
	}

	public void addMap(String code) {
		ServerMap f = null;
		for (ServerMap m : mapsLeft) {
			if (m.toString().equals(code)) {
				f = m;
			}
		}
		if (f != null) {
			mapsLeft.remove(f);
			mapsRight.add(f);
			settings.setCurrentStatus();
			resortMaps();
		}
	}

	private static final Comparator<ServerMap> leftCmp = new Comparator<ServerMap>() {

		@Override
		public int compare(ServerMap o1, ServerMap o2) {
			return o1.toString().compareTo(o2.toString());
		}

	};

	private void resortMaps() {
		Collections.sort(mapsLeft, leftCmp);
		List<String> l = new ArrayList<>();
		for (ServerMap m : mapsLeft) {
			l.add(m.getMapCode());
		}
		List<String> r = new ArrayList<>();
		for (ServerMap m : mapsRight) {
			r.add(m.getMapCode());
		}
		settings.setLeftMaps(l);
		settings.setRightMaps(r);
		settings.save();
	}

	public boolean validate() {
		boolean v = true;
		List<ServerMap> maps = mapsLeft;
		for (Map m : maps) {
			try {
				m.validate();
			} catch (InvalidMapException e) {
				v = false;
			}
		}
		maps = mapsRight;
		for (Map m : maps) {
			try {
				m.validate();
			} catch (InvalidMapException e) {
				v = false;
			}
		}
		return v;
	}

	public ServerMap getMapByCode(String mcod) {
		ServerMap f = null;
		for (ServerMap m : mapsLeft) {
			if (m.toString().equals(mcod)) {
				f = m;
			}
		}
		if (f != null) {
			return f;
		}
		for (ServerMap m : mapsRight) {
			if (m.toString().equals(mcod)) {
				f = m;
			}
		}
		if (f != null) {
			return f;
		}
		return null;
	}

	public int getLeftMapsCount() {
		return mapsLeft.size();
	}

	public String getLeftMapAt(int index) {
		return mapsLeft.get((int) index).toString();
	}

	public int getRightMapsCount() {
		return mapsRight.size();
	}

	public String getRightMapAt(int index) {
		return mapsRight.get((int) index).toString();
	}

	public void reloadMaps() {
		detectMaps();
		handler.updateMapLists();
	}

	public void moveItemsUp(int[] items) {
		for (int i : items) {
			ServerMap prev = mapsRight.get(i - 1);
			mapsRight.set(i - 1, mapsRight.get(i));
			mapsRight.set(i, prev);
			resortMaps();
		}
	}

	public void moveItemsDown(int[] items) {
		for (int i : items) {
			ServerMap prev = mapsRight.get(i + 1);
			mapsRight.set(i + 1, mapsRight.get(i));
			mapsRight.set(i, prev);
			resortMaps();
		}
	}

	public List<ServerMap> getRightMaps() {
		return mapsRight;
	}

	public boolean isRunning() {
		return this.processRunning();
	}
}
