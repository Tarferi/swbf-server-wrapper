package org.spigot.swbfserver.servers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.spigot.swbfserver.Settings;
import org.spigot.swbfserver.GUI.MainGUI.serverGuiManager;
import org.spigot.swbfserver.maps.Map;
import org.spigot.swbfserver.maps.MapDetector;

public class ServerManager {

	private static Settings settings;
	private static serverGuiManager guimanager;
	private static List<Map> maps = new ArrayList<>();
	private static List<String> mapsByCodes = new ArrayList<>();
	private static HashMap<ServerSettings, Server> servers = new HashMap<>();
	private static List<Map> mapsDis = new ArrayList<>();
	private static List<Map> mapsEna;

	public static Server addServer(ServerSettings set) {
		Server s = new Server(set);
		servers.put(set, s);
		settings.addServer(s);
		return s;
	}

	public static void removeServer(Server s) {
		settings.removeServer(s);
		guimanager.removeServer(s);
		servers.remove(s.getSettings());
	}

	public static void updateServerName(ServerSettings s) {
		guimanager.updateServerName(servers.get(s));
	}

	private static void loadSettings() {
		settings = Settings.loadSettings();
	}

	private static void loadServers() {
		List<ServerSettings> servers = settings.getServers();
		List<ServerSettings> newServers = new ArrayList<>();
		for (ServerSettings s : servers) {
			Server ss = new Server(s);
			ServerManager.servers.put(s, ss);
			if (ss.validate()) {
				newServers.add(ss.getSettings());
				guimanager.addServer(ss);
			}
		}
		settings.setServers(newServers);
	}

	public static void construct() {
		settings = new Settings();
		loadSettings();
		loadMaps();
		guimanager.addMainPanel();
		guimanager.addCatalogPanel();
		guimanager.addMapManagerPanel();
		loadServers();
	}

	private static void loadMaps() {
		maps = MapDetector.getMaps();
		mapsDis = MapDetector.getDisabledMaps();
		mapsEna = MapDetector.getEnabledMaps();
		mapsByCodes.clear();
		for (Map m : maps) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
		for (Map m : mapsDis) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
		for (Map m : mapsEna) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
	}

	public static void saveSettings() {
		settings.saveSettings();
	}

	public static void setMainGuiManager(serverGuiManager manager) {
		guimanager = manager;
	}

	public static List<Map> getDisabledMapList() {
		return mapsDis;
	}

	public static List<Map> getEnabledMapList() {
		return mapsEna;
	}

	public static List<Map> getMapList() {
		return maps;
	}

	public static String getServerFolderPath() {
		return getParentPath(settings.getServerPath());
	}

	private static String getParentPath(String s) {
		s = s.replace("\\", "/");
		if (s.contains("/")) {
			s = s.substring(0, s.lastIndexOf("/"));
		}
		return s;
	}

	public static String getServerPath() {
		return settings.getServerPath();
	}

	public static void setServerPath(String text) {
		settings.setServerPath(text);

	}

	public static void reloadMaps() {
		loadMaps();
		for (Entry<ServerSettings, Server> data : servers.entrySet()) {
			data.getValue().reloadMaps();
		}
	}

	public static boolean getMapByCode(String code) {
		code = code.substring(0, code.length() - 1);
		return getMapByPureCode(code);
	}

	public static boolean getMapByPureCode(String code) {
		return mapsByCodes.contains(code.toLowerCase());
	}

	public static void disableMap(Map m) {
		MapDetector.disableMap(m);
		ServerManager.reloadMaps();
	}

	public static void enableMap(Map m) {
		MapDetector.enableMap(m);
		ServerManager.reloadMaps();
	}

	public static String getMapOrigin(Map map) {
		return MapDetector.getMapOrigin(map);
	}

	public static void setMapOrigin(String targetfile, String url) {
		MapDetector.setMapOrigin(targetfile, url);
	}

	public static void setPreArguments(String p) {
		settings.setPreArguments(p);
	}

	public static String getPreArguments() {
		return settings.getPreArguments();
	}

	public static void stoptAllServers() {
		for (Server s : servers.values()) {
			if (!s.isRunning()) {
				s.stop();
			}
		}
	}

	public static void startAllServers() {
		for (Server s : servers.values()) {
			if (!s.isRunning()) {
				s.start();
			}
		}
	}

	public static void restartAllServers() {
		for (Server s : servers.values()) {
			if (!s.isRunning()) {
				s.restart();
			}
		}
	}
}
