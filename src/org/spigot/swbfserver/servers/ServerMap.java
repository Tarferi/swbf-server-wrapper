package org.spigot.swbfserver.servers;

import java.io.Serializable;

import org.spigot.swbfserver.maps.Map;

public class ServerMap extends Map implements Serializable {

	private static final long serialVersionUID = 3404711412032223591L;

	public ServerMap(Map m) {
		super(m.getMapCode(), m.getReinforcements1(), m.getReinforcements2(), m.getPlanet(), m.getBattle());
	}

	public void setReinforcements1(int rein1) {
		super.setRein1(rein1);
	}

	public void setReinforcements2(int rein2) {
		super.setRein2(rein2);
	}
}
