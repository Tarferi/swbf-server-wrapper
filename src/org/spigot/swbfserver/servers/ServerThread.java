package org.spigot.swbfserver.servers;

public class ServerThread {

	public ServerThread(final Runnable r) {
		new Thread() {
			@Override
			public void run() {
				setName("Server thread");
				r.run();
			}
		}.start();
	}
}
