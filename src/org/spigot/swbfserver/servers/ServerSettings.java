package org.spigot.swbfserver.servers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.spigot.swbfserver.GUI.ServerPanel.serverStatusUpdateHandler;
import org.spigot.swbfserver.maps.Map;

public class ServerSettings implements Serializable {

	private static final long serialVersionUID = -7649848312720479950L;
	private String name = "Test Server", password = "";
	private int minplayers = 2, maxplayers = 10, aiperteam = 5, spawninvincibility = 0;
	private boolean hasPass = false;
	private boolean autoSelect = false;
	private int aidiff = 1, port = 123;
	private boolean heroes = false, teamdamage = false, shownames = true, autorestart = false, randommaporder = false;

	private List<String> mapCodesLeft = new ArrayList<>();

	private List<String> mapCodesRight = new ArrayList<>();

	private List<Map> mappCodesLeft = new ArrayList<>();

	private List<Map> mappCodesRight = new ArrayList<>();

	public List<Map> getMapCodesLeft() {
		return mappCodesLeft;
	}

	public List<Map> getMapCodesRight() {
		return mappCodesRight;
	}

	public void validate() {
		HashMap<String, Map> tmp = new HashMap<>();
		List<Map> maps = ServerManager.getMapList();
		for (Map m : maps) {
			tmp.put(m.getMapCode(), m);
		}
		List<String> mapss = new ArrayList<>();
		mappCodesLeft.clear();
		removeDuplicates(mapCodesLeft);
		removeDuplicates(mapCodesRight);
		for (String s : mapCodesLeft) {
			if (tmp.containsKey(s)) {
				mappCodesLeft.add(tmp.get(s));
				mapss.add(s);
			}
		}
		mapCodesLeft = mapss;
		mappCodesRight.clear();
		mapss = new ArrayList<>();
		for (String s : mapCodesRight) {
			if (tmp.containsKey(s)) {
				mappCodesRight.add(tmp.get(s));
				mapss.add(s);
			}
		}
		mapCodesRight = mapss;
		for (Map m : maps) {
			if (!hasMap(m)) {
				mappCodesLeft.add(m);
			}
		}
	}

	private void removeDuplicates(List<String> al) {
		Set<String> hs = new HashSet<>();
		hs.addAll(al);
		al.clear();
		al.addAll(hs);
	}

	private boolean hasMap(Map map) {
		return mappCodesLeft.contains(map) || mappCodesRight.contains(map);
	}

	public void unvalidate() {
		mappCodesLeft.clear();
		mappCodesRight.clear();
	}

	private serverStatusUpdateHandler handler;

	public void setUpdateHandler(serverStatusUpdateHandler handler) {
		this.handler = handler;
	}

	public void setName(String name) {
		this.name = name;
		setCurrentStatus();
		ServerManager.updateServerName(this);
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
		setCurrentStatus();
	}

	public boolean hasRandomMapOrder() {
		return randommaporder;
	}

	public boolean hasAutoRestartEnabled() {
		return autorestart;
	}

	public boolean showPlayerNames() {
		return shownames;
	}

	public boolean hasTeamDamage() {
		return teamdamage;
	}

	public boolean hasHeroes() {
		return heroes;
	}

	public int getAiDifficulty() {
		return aidiff;
	}

	public boolean isTeamAutoSelect() {
		return autoSelect;
	}

	public int getSpawnInvincibility() {
		return spawninvincibility;
	}

	public int getAiPerTeam() {
		return aiperteam;
	}

	public int getMinPlayers() {
		return minplayers;
	}

	public int getMaxPlayers() {
		return maxplayers;
	}

	public String getServerName() {
		return name;
	}

	public boolean hasPassword() {
		return hasPass;
	}

	public String getPassword() {
		return password == null ? "" : password;
	}

	public void setPassword(String password) {
		this.password = password;
		setCurrentStatus();
	}

	public void setPassword(boolean pass) {
		hasPass = pass;
		setCurrentStatus();
	}

	public void setMinPlayers(int players) {
		this.minplayers = players;
		setCurrentStatus();
	}

	public void setMaxPlayers(int players) {
		this.maxplayers = players;
		setCurrentStatus();
	}

	public void setAiPerTeam(int aiperteam) {
		this.aiperteam = aiperteam;
		setCurrentStatus();
	}

	public void setSpawnInvincibility(int inv) {
		this.spawninvincibility = inv;
		setCurrentStatus();
	}

	protected void updateStatus(String status) {
		if (handler != null) {
			handler.setStatus(status);
		}
	}

	public void setTeamSelect(boolean autoselect) {
		this.autoSelect = autoselect;
		setCurrentStatus();
	}

	public final void save() {
		ServerManager.saveSettings();
	}

	protected void setCurrentStatus() {
		updateStatus("Preparing server settings");
		save();
	}

	public void setAiDifficulty(int diff) {
		this.aidiff = diff;
		setCurrentStatus();
	}

	public void setHeroes(boolean heroes) {
		this.heroes = heroes;
		setCurrentStatus();
	}

	public void setTeamDamage(boolean td) {
		this.teamdamage = td;
		setCurrentStatus();
	}

	public void setShowPlayerName(boolean show) {
		this.shownames = show;
		setCurrentStatus();
	}

	public void setAutoRestart(boolean ar) {
		this.autorestart = ar;
		setCurrentStatus();
	}

	public void setRandomMapOrder(boolean rmo) {
		this.randommaporder = rmo;
		setCurrentStatus();
	}

	public void setLeftMaps(List<String> sortedLeft) {
		mapCodesLeft = sortedLeft;
	}

	public void setRightMaps(List<String> sortedRight) {
		mapCodesRight = sortedRight;
	}

}
