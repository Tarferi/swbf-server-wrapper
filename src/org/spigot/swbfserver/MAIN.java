package org.spigot.swbfserver;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.spigot.swbfserver.GUI.MainGUI;
import org.spigot.swbfserver.extractorAdapter.ExtractorAdapter;
import org.spigot.swbfserver.servers.ServerManager;

public class MAIN {


	private static final File tmpfolder = new File(getCurrentFolder().getAbsolutePath() + "/tmp/");

	public static File getTempFile() {
		tmpfolder.mkdirs();
		File f = new File(tmpfolder.getAbsolutePath() + "/" + UUID.randomUUID().toString());
		return f;
	}

	public static java.io.File getCurrentFolder() {
		try {
			return new java.io.File(MAIN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static final String version = "1.3 Beta";

	public static void main(String[] args) {
		if (ExtractorAdapter.loadLibrary()) {
			load();
		}
	}

	private static final void load() {
		new MainGUI();
		ServerManager.construct();
	}

	public static final Image ImageLoader(String filename) {
		Image qg = null;
		try {
			URL src = MainGUI.class.getResource(filename);
			if (src != null) {
				qg = ImageIO.read(src);
			} else {
				qg = new BufferedImage(20, 20, 1);
				System.err.println("NULL IMAGE " + filename);
			}
		} catch (Exception e) {
			e.printStackTrace();
			qg = new BufferedImage(20, 20, 1);
		}
		return qg;
	}

	public static String getLibraryURI() {
		return "http://swbf.ithief.net/Extractor.jar";
	}
}
