package org.spigot.swbfserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.spigot.swbfserver.servers.Server;
import org.spigot.swbfserver.servers.ServerSettings;

public class Settings implements Serializable {

	private static final String SettingsFile = "ServerSettings.bin";

	private String swbfpath = "C:\\Program Files\\LucasArts\\Star Wars Battlefront PC Server\\battlefront server.exe";

	private static File getCurrentFolder() {
		try {
			return new File(MAIN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getServerPath() {
		return swbfpath;
	}

	private static File getSettingsFile() {
		File f = getCurrentFolder();
		if (f == null) {
			return new File(SettingsFile);
		} else {
			return new File(f.getAbsolutePath() + "/" + SettingsFile);
		}
	}

	private static final long serialVersionUID = 5843479721199438512L;

	private List<ServerSettings> servers = new ArrayList<>();

	private String preargs;

	public static Settings loadSettings() {
		File f = getSettingsFile();
		InputStream out = null;
		ObjectInputStream oout = null;
		Object o = null;
		try {
			out = new FileInputStream(f);
		} catch (Exception e) {
			try {
				out.close();
			} catch (Exception ee) {
			}
			return new Settings();
		}
		try {
			oout = new ObjectInputStream(out);
		} catch (Exception e) {
			try {
				out.close();
			} catch (Exception ee) {
			}
			try {
				oout.close();
			} catch (Exception ee) {
			}
			return new Settings();
		}
		try {
			o = oout.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (Exception ee) {
		}
		try {
			oout.close();
		} catch (Exception ee) {
		}
		if (o != null) {
			if (o instanceof Settings) {
				Settings s = (Settings) o;
				return s;
			}
		}
		return new Settings();
	}

	public void saveSettings() {
		File f = getSettingsFile();
		OutputStream out = null;
		ObjectOutputStream oout = null;
		try {
			out = new FileOutputStream(f);
		} catch (Exception e) {
			try {
				out.close();
			} catch (Exception ee) {
			}
			return;
		}
		try {
			oout = new ObjectOutputStream(out);
		} catch (Exception e) {
			try {
				out.close();
			} catch (Exception ee) {
			}
			try {
				oout.close();
			} catch (Exception ee) {
			}
			return;
		}
		try {
			oout.writeObject(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (Exception ee) {
		}
		try {
			oout.close();
		} catch (Exception ee) {
		}
	}

	public void addServer(Server s) {
		servers.add(s.getSettings());
		saveSettings();
	}

	public void removeServer(Server s) {
		servers.remove(s.getSettings());
		saveSettings();
	}

	public List<ServerSettings> getServers() {
		return servers;
	}

	public void setServers(List<ServerSettings> newServers) {
		servers = newServers;
	}

	public void setServerPath(String text) {
		if (text == null) {
			text = "";
		}
		swbfpath = text;
		saveSettings();
	}

	public void setPreArguments(String p) {
		preargs = p;
	}

	public String getPreArguments() {
		return preargs == null ? "" : preargs;
	}

}
