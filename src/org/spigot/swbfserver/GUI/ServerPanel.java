package org.spigot.swbfserver.GUI;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import javax.swing.UIManager;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JRadioButton;

import org.spigot.swbfserver.maps.Era;
import org.spigot.swbfserver.servers.Server;
import org.spigot.swbfserver.servers.ServerMap;
import org.spigot.swbfserver.utils.IntegerUtils;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.Serializable;

import javax.swing.AbstractListModel;

public class ServerPanel extends JPanel {

	private static final long serialVersionUID = 7208508446451891118L;
	private JTextField textField;
	private JTextField txtTestServer;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JLabel lblserverdescr;

	private final List<String> selectedRight = new ArrayList<>();
	private final List<String> selectedLeft = new ArrayList<>();

	private JRadioButton rdbtnPlayerSelect;
	private JRadioButton rdbtnAutoAssign;
	private JRadioButton rdbtnEasy;
	private JRadioButton rdbtnMedium;
	private JRadioButton rdbtnHard;
	private JList<String> listLeft;
	private JList<String> listRight;
	private Server server;
	private JButton btnStop;
	private JButton btnRestart;
	private JButton btnStart;
	private JPanel panel_7;
	private JPanel panel_8;
	private JLabel lblRebels;
	private JLabel lblEmpire;
	private JLabel lblCis;
	private JLabel lblRepublic;
	private JPanel panel_10;
	private JButton buttonUp;
	private JButton buttonDown;

	/**
	 * Create the panel.
	 */
	public ServerPanel(final Server s) {
		this.server = s;
		setLayout(new BorderLayout(0, 0));
		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new MigLayout("", "[356px][grow]", "[86.00px][177.00,grow][]"));

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "General settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_2, "cell 0 0,grow");
		panel_2.setLayout(new MigLayout("", "[85px][261px]", "[20px][20px][20px][21px]"));

		JLabel lblServerPort = new JLabel("Server port:");
		panel_2.add(lblServerPort, "cell 0 0,alignx right,aligny center");

		textField = new JTextField();
		textField.setEditable(false);
		textField.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				int i = IntegerUtils.parseIntOrDefault(textField.getText(), -1);
				if (i <= 0 || i >= 65536) {
					e.getEdit().undo();
				} else {
					s.getSettings().setPort(i);
				}
			}

		});
		panel_2.add(textField, "cell 1 0,growx,aligny top");
		textField.setText(s.getSettings().getPort() + "");
		textField.setColumns(10);

		JLabel lblServerName = new JLabel("Server Name:");
		panel_2.add(lblServerName, "cell 0 1,alignx right,aligny center");

		txtTestServer = new JTextField();
		panel_2.add(txtTestServer, "cell 1 1,growx,aligny top");
		txtTestServer.setText(s.getSettings().getServerName());
		txtTestServer.setColumns(10);
		txtTestServer.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				String sname = txtTestServer.getText();
				int len = sname.length();
				if (len <= 0 || len > 64) {
					e.getEdit().undo();
				} else {
					s.getSettings().setName(sname);
				}
			}

		});

		JLabel lblServerPassword = new JLabel("Server Password:");
		panel_2.add(lblServerPassword, "cell 0 2,alignx right,aligny center");

		textField_1 = new JTextField(s.getSettings().getPassword());
		panel_2.add(textField_1, "cell 1 2,growx,aligny top");
		textField_1.setColumns(10);
		textField_1.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				String spass = textField_1.getText();
				int len = spass.length();
				if (len > 64) {
					e.getEdit().undo();
				} else {
					s.getSettings().setPassword(spass);
				}
			}

		});

		JLabel lblUsePassword = new JLabel("Use password:");
		panel_2.add(lblUsePassword, "cell 0 3,alignx right,aligny center");

		JCheckBox checkBox = new JCheckBox("");
		checkBox.setSelected(s.getSettings().hasPassword());
		panel_2.add(checkBox, "cell 1 3,growx,aligny top");

		JPanel panel_9 = new JPanel();
		panel_9.setBorder(new TitledBorder(null, "Server ", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_9, "cell 1 0,grow");
		panel_9.setLayout(new MigLayout("", "[30%][30%][30%]", "[][]"));

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart.setEnabled(false);
				btnStop.setEnabled(false);
				btnRestart.setEnabled(false);
				s.start();
			}
		});
		panel_9.add(btnStart, "cell 0 0,grow");

		btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		panel_9.add(btnStop, "cell 1 0,grow");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart.setEnabled(false);
				btnStop.setEnabled(false);
				btnRestart.setEnabled(false);
				s.stop();
			}
		});

		btnRestart = new JButton("Restart");
		btnRestart.setEnabled(false);
		panel_9.add(btnRestart, "cell 2 0,grow");
		btnRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStart.setEnabled(false);
				btnStop.setEnabled(false);
				btnRestart.setEnabled(false);
				s.restart();
			}
		});

		final JCheckBox chckbxAutorestart = new JCheckBox("Autorestart");
		chckbxAutorestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s.getSettings().setAutoRestart(chckbxAutorestart.isSelected());
			}
		});
		chckbxAutorestart.setSelected(s.getSettings().hasAutoRestartEnabled());
		panel_9.add(chckbxAutorestart, "cell 0 1 3 1");

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Maps", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_3, "cell 0 1 2 1,grow");
		panel_3.setLayout(new MigLayout("", "[40%][grow][][40%]", "[grow]"));

		JScrollPane scrollPane = new JScrollPane();
		panel_3.add(scrollPane, "cell 0 0,grow");

		listLeft = new JList<>();
		listLeft.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				List<String> lst = listLeft.getSelectedValuesList();
				selectedLeft.clear();
				for (String o : lst) {
					selectedLeft.add(o);
				}
			}

		});
		scrollPane.setViewportView(listLeft);

		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4, "cell 1 0,growx,aligny center");
		panel_4.setLayout(new MigLayout("", "[grow]", "[][]"));

		JButton button = new JButton(">>");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (String s : selectedLeft) {
					server.addMap(s);
				}
				updateLeftList();
				updateRightList();
			}
		});
		panel_4.add(button, "cell 0 0,growx");

		JButton button_1 = new JButton("<<");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (String s : selectedRight) {
					server.removeMap(s);
				}
				updateLeftList();
				updateRightList();
			}
		});
		panel_4.add(button_1, "cell 0 1,growx");

		panel_10 = new JPanel();
		panel_3.add(panel_10, "cell 2 0,growx,aligny center");
		panel_10.setLayout(new MigLayout("", "[]", "[][]"));

		buttonUp = new JButton("\u2191");
		buttonUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] cs = listRight.getSelectedIndices();
				server.moveItemsUp(cs);
				updateRightList();
				for (int i = 0; i < cs.length; i++) {
					cs[i]--;
				}
				listRight.setSelectedIndices(cs);
			}
		});
		buttonUp.setEnabled(false);
		panel_10.add(buttonUp, "cell 0 0,grow");

		buttonDown = new JButton("\u2193");
		buttonDown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] cs = listRight.getSelectedIndices();
				server.moveItemsDown(cs);
				updateRightList();
				for (int i = 0; i < cs.length; i++) {
					cs[i]++;
				}
				listRight.setSelectedIndices(cs);
			}
		});
		buttonDown.setEnabled(false);
		panel_10.add(buttonDown, "cell 0 1,alignx center,growy");

		JScrollPane scrollPane_1 = new JScrollPane();
		panel_3.add(scrollPane_1, "cell 3 0,grow");

		listRight = new JList<>();
		scrollPane_1.setViewportView(listRight);
		listRight.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				handleArrows(listRight.getSelectedIndices(), listRight.getModel().getSize());
				List<String> lst = listRight.getSelectedValuesList();
				selectedRight.clear();
				boolean cw = false;
				boolean gcw = false;
				String rein1a = "";
				String rein1b = "";
				String rein2a = "";
				String rein2b = "";

				if (lst.size() == 1) {
					ServerMap m = s.getMapByCode(lst.get(0));
					if (m.getEra() == Era.CW) {
						cw = true;
						rein1a = m.getReinforcements1() + "";
						rein2a = m.getReinforcements2() + "";
					} else {
						gcw = true;
						rein1b = m.getReinforcements1() + "";
						rein2b = m.getReinforcements2() + "";
					}
				}
				textField_5.setText(rein1a);
				textField_6.setText(rein2a);
				textField_7.setText(rein1b);
				textField_8.setText(rein2b);
				textField_5.setEnabled(cw);
				textField_6.setEnabled(cw);
				textField_7.setEnabled(gcw);
				textField_8.setEnabled(gcw);
				lblRepublic.setEnabled(cw);
				panel_7.setEnabled(cw);
				panel_8.setEnabled(gcw);
				lblCis.setEnabled(cw);
				lblEmpire.setEnabled(gcw);
				lblRebels.setEnabled(gcw);
				for (String o : lst) {
					selectedRight.add(o);
				}
			}

		});

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Common Maps Settings", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.add(panel_5, "cell 0 2,grow");
		panel_5.setLayout(new MigLayout("", "[119px][97px][69px][49px]", "[23px][23px][23px][23px][23px][23px]"));

		final JCheckBox chckbxHeroes = new JCheckBox("Heroes");
		chckbxHeroes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s.getSettings().setHeroes(chckbxHeroes.isSelected());
			}
		});
		chckbxHeroes.setSelected(s.getSettings().hasHeroes());
		panel_5.add(chckbxHeroes, "cell 0 0,growx,aligny top");

		JLabel lblNewLabel = new JLabel("Player limit:");
		panel_5.add(lblNewLabel, "cell 1 0,alignx right,aligny center");

		textField_2 = new JTextField(s.getSettings().getMaxPlayers() + "");
		panel_5.add(textField_2, "cell 2 0 2 1,growx,aligny center");
		textField_2.setColumns(10);
		textField_2.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				int i = IntegerUtils.parseIntOrDefault(textField_2.getText(), -1);
				if (i <= 0 || i >= 64) {
					e.getEdit().undo();
				} else {
					s.getSettings().setMaxPlayers(i);
				}
			}

		});

		final JCheckBox chckbxNewCheckBox = new JCheckBox("Team Damage");
		chckbxNewCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s.getSettings().setTeamDamage(chckbxNewCheckBox.isSelected());
			}
		});
		chckbxNewCheckBox.setSelected(s.getSettings().hasTeamDamage());
		panel_5.add(chckbxNewCheckBox, "cell 0 1,growx,aligny top");

		JLabel lblAiPerTeam = new JLabel("Ai per team:");
		panel_5.add(lblAiPerTeam, "cell 1 1,alignx right,aligny center");

		textField_3 = new JTextField(s.getSettings().getAiPerTeam() + "");
		panel_5.add(textField_3, "cell 2 1 2 1,growx,aligny center");
		textField_3.setColumns(10);
		textField_3.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				int i = IntegerUtils.parseIntOrDefault(textField_3.getText(), -1);
				if (i < 0 || i >= 32) {
					e.getEdit().undo();
				} else {
					s.getSettings().setAiPerTeam(i);
				}
			}

		});

		final JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Show Player Names");
		chckbxNewCheckBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s.getSettings().setShowPlayerName(chckbxNewCheckBox_1.isSelected());
			}
		});
		chckbxNewCheckBox_1.setSelected(s.getSettings().showPlayerNames());
		panel_5.add(chckbxNewCheckBox_1, "cell 0 2,alignx left,aligny top");

		JLabel lblMinPlayersTo = new JLabel("Min players to start:");
		panel_5.add(lblMinPlayersTo, "cell 1 2,alignx right,aligny center");

		textField_4 = new JTextField(s.getSettings().getMinPlayers() + "");
		panel_5.add(textField_4, "cell 2 2 2 1,growx,aligny center");
		textField_4.setColumns(10);
		textField_4.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				int i = IntegerUtils.parseIntOrDefault(textField_4.getText(), -1);
				if (i < 0 || i >= 64) {
					e.getEdit().undo();
				} else {
					s.getSettings().setMinPlayers(i);
				}
			}

		});

		final JCheckBox chckbxRandomMapOrder = new JCheckBox("Random Map Order");
		chckbxRandomMapOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				s.getSettings().setRandomMapOrder(chckbxRandomMapOrder.isSelected());
			}
		});
		chckbxRandomMapOrder.setSelected(s.getSettings().hasRandomMapOrder());
		panel_5.add(chckbxRandomMapOrder, "cell 0 3,alignx left,aligny top");

		JLabel lblSpawnInvincibility = new JLabel("Spawn Invincibility:");
		panel_5.add(lblSpawnInvincibility, "cell 1 3,alignx right,aligny center");

		textField_9 = new JTextField(s.getSettings().getSpawnInvincibility() + "");
		panel_5.add(textField_9, "cell 2 3 2 1,growx,aligny center");
		textField_9.setColumns(10);
		textField_9.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				int i = IntegerUtils.parseIntOrDefault(textField_9.getText(), -1);
				if (i < 0 || i >= 60) {
					e.getEdit().undo();
				} else {
					s.getSettings().setSpawnInvincibility(i);
				}
			}

		});

		JLabel lblTeamAssign = new JLabel("Team Assign:");
		panel_5.add(lblTeamAssign, "cell 0 4,alignx right,aligny center");

		rdbtnPlayerSelect = new JRadioButton("Player Select");
		rdbtnPlayerSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnPlayerSelect.isSelected()) {
					rdbtnAutoAssign.setSelected(false);
					s.getSettings().setTeamSelect(false);
				}
			}
		});
		rdbtnPlayerSelect.setSelected(!s.getSettings().isTeamAutoSelect());
		panel_5.add(rdbtnPlayerSelect, "cell 1 4,growx,aligny top");

		rdbtnAutoAssign = new JRadioButton("Auto Assign");
		panel_5.add(rdbtnAutoAssign, "cell 2 4 2 1,growx,aligny top");
		rdbtnAutoAssign.setSelected(s.getSettings().isTeamAutoSelect());
		rdbtnAutoAssign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnAutoAssign.isSelected()) {
					rdbtnPlayerSelect.setSelected(false);
					s.getSettings().setTeamSelect(true);
				}
			}
		});
		JLabel lblAiDifficulty = new JLabel("AI Difficulty:");
		panel_5.add(lblAiDifficulty, "cell 0 5,alignx right,aligny center");
		rdbtnEasy = new JRadioButton("Easy");
		rdbtnEasy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnEasy.isSelected()) {
					rdbtnMedium.setSelected(false);
					rdbtnHard.setSelected(false);
					s.getSettings().setAiDifficulty(1);
				}
			}
		});
		panel_5.add(rdbtnEasy, "cell 1 5,alignx right,aligny top");

		rdbtnMedium = new JRadioButton("Medium");
		rdbtnMedium.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnMedium.isSelected()) {
					rdbtnHard.setSelected(false);
					rdbtnEasy.setSelected(false);
					s.getSettings().setAiDifficulty(2);
				}
			}
		});
		panel_5.add(rdbtnMedium, "cell 2 5,growx,aligny top");

		rdbtnHard = new JRadioButton("Hard");
		rdbtnHard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rdbtnHard.isSelected()) {
					rdbtnMedium.setSelected(false);
					rdbtnEasy.setSelected(false);
					s.getSettings().setAiDifficulty(3);
				}
			}
		});
		rdbtnEasy.setSelected(s.getSettings().getAiDifficulty() == 1);
		rdbtnMedium.setSelected(s.getSettings().getAiDifficulty() == 2);
		rdbtnHard.setSelected(s.getSettings().getAiDifficulty() == 3);
		panel_5.add(rdbtnHard, "cell 3 5,alignx left,aligny top");

		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(null, "Map Specific Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_6, "cell 1 2,grow");
		panel_6.setLayout(new MigLayout("", "[50%][50%]", "[grow]"));

		panel_7 = new JPanel();
		panel_6.add(panel_7, "cell 0 0,grow");
		panel_7.setLayout(new MigLayout("", "[][grow]", "[][][]"));

		lblRepublic = new JLabel("Republic:");
		lblRepublic.setEnabled(false);
		panel_7.add(lblRepublic, "cell 0 0,alignx trailing");

		textField_5 = new JTextField();
		textField_5.setEnabled(false);
		panel_7.add(textField_5, "cell 1 0,growx");
		textField_5.setColumns(10);
		((PlainDocument) textField_5.getDocument()).setDocumentFilter(new ReinforcementsFilter(new reinforcementsHandler() {

			@Override
			public void set(int value) {
				for (String code : selectedRight) {
					ServerMap m = s.getMapByCode(code);
					if (m != null) {
						m.setReinforcements1(value);
					}
				}
			}

		}));

		lblCis = new JLabel("CIS:");
		lblCis.setEnabled(false);
		panel_7.add(lblCis, "cell 0 1,alignx trailing");

		textField_6 = new JTextField();
		textField_6.setEnabled(false);
		panel_7.add(textField_6, "cell 1 1,growx");
		textField_6.setColumns(10);
		((PlainDocument) textField_6.getDocument()).setDocumentFilter(new ReinforcementsFilter(new reinforcementsHandler() {

			@Override
			public void set(int value) {
				for (String code : selectedRight) {
					ServerMap m = s.getMapByCode(code);
					if (m != null) {
						m.setReinforcements2(value);
					}
				}
			}
		}));

		panel_8 = new JPanel();
		panel_6.add(panel_8, "cell 1 0,grow");
		panel_8.setLayout(new MigLayout("", "[][grow]", "[][]"));

		lblRebels = new JLabel("Rebels:");
		lblRebels.setEnabled(false);
		panel_8.add(lblRebels, "cell 0 0,alignx trailing");

		textField_7 = new JTextField();
		textField_7.setEnabled(false);
		panel_8.add(textField_7, "cell 1 0,growx");
		textField_7.setColumns(10);
		((PlainDocument) textField_7.getDocument()).setDocumentFilter(new ReinforcementsFilter(new reinforcementsHandler() {

			@Override
			public void set(int value) {
				for (String code : selectedRight) {
					ServerMap m = s.getMapByCode(code);
					if (m != null) {
						m.setReinforcements1(value);
					}
				}
			}
		}));

		lblEmpire = new JLabel("Empire");
		lblEmpire.setEnabled(false);
		panel_8.add(lblEmpire, "cell 0 1,alignx trailing");

		textField_8 = new JTextField();
		textField_8.setEnabled(false);
		panel_8.add(textField_8, "cell 1 1,growx");
		textField_8.setColumns(10);
		((PlainDocument) textField_8.getDocument()).setDocumentFilter(new ReinforcementsFilter(new reinforcementsHandler() {

			@Override
			public void set(int value) {
				for (String code : selectedRight) {
					ServerMap m = s.getMapByCode(code);
					if (m != null) {
						m.setReinforcements2(value);
					}
				}
			}
		}));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new MigLayout("", "[]", "[]"));

		lblserverdescr = new JLabel("Server loaded");
		panel.add(lblserverdescr, "cell 0 0,grow");
		s.setUpdateHandler(new serverStatusUpdateHandlerWrapper());
		updateLeftList();
		updateRightList();
	}

	protected void handleArrows(int[] s, int size) {
		if (s.length > 0) {
			int first = s[0];
			int last = s[s.length - 1];
			buttonUp.setEnabled(first > 0);
			buttonDown.setEnabled(last + 1 < size);
		} else {
			buttonUp.setEnabled(false);
			buttonDown.setEnabled(false);
		}
	}

	private void updateLeftList() {
		selectedLeft.clear();
		listLeft.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = -4989781965726628491L;

			public int getSize() {
				return server.getLeftMapsCount();
			}

			public String getElementAt(int index) {
				return server.getLeftMapAt(index);
			}
		});
		listLeft.repaint();
	}

	private void updateRightList() {
		selectedRight.clear();
		listRight.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = 3519066452252146746L;

			public int getSize() {
				return server.getRightMapsCount();
			}

			public String getElementAt(int index) {
				return server.getRightMapAt(index);
			}
		});
		listRight.repaint();
	}

	public class serverStatusUpdateHandlerWrapper {
		private serverStatusUpdateHandler handler;

		private serverStatusUpdateHandlerWrapper() {
			handler = new serverStatusUpdateHandler(lblserverdescr);
		}

		public void setServerStopped() {
			btnRestart.setEnabled(false);
			btnStart.setEnabled(true);
			btnStop.setEnabled(false);
		}

		public serverStatusUpdateHandler getHandler() {
			return handler;
		}

		public void updateMapLists() {
			updateLeftList();
			updateRightList();
		}

		public void setServerStarted() {
			btnRestart.setEnabled(true);
			btnStart.setEnabled(false);
			btnStop.setEnabled(true);
		}
	}

	public static class serverStatusUpdateHandler implements Serializable {
		private static final long serialVersionUID = -8702126829582042319L;
		private JLabel l;

		private serverStatusUpdateHandler(JLabel l) {
			this.l = l;
		}

		public synchronized void setStatus(String status) {
			l.setText(status);
		}

	}

	private interface reinforcementsHandler {
		public void set(int value);
	}

	private class ReinforcementsFilter extends DocumentFilter {

		private reinforcementsHandler r;

		private ReinforcementsFilter(reinforcementsHandler r) {
			this.r = r;
		}

		@Override
		public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.insert(offset, string);
			if (test(sb.toString())) {
				super.insertString(fb, offset, string, attr);
			}
		}

		private boolean test(String text) {
			try {
				int i = Integer.parseInt(text);
				if (i > 0 && i < 1000) {
					r.set(i);
					return true;
				} else {
					return false;
				}
			} catch (NumberFormatException e) {
				return false;
			}
		}

		@Override
		public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {

			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.replace(offset, offset + length, text);
			if (test(sb.toString())) {
				super.replace(fb, offset, length, text, attrs);
			}

		}

		@Override
		public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.delete(offset, offset + length);
			if (test(sb.toString())) {
				super.remove(fb, offset, length);
			}

		}
	}

}
