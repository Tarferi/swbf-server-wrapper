package org.spigot.swbfserver.GUI;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JScrollPane;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.miginfocom.swing.MigLayout;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import org.spigot.swbfserver.MAIN;
import org.spigot.swbfserver.maps.catalog.CatalogMap;
import org.spigot.swbfserver.maps.catalog.CatalogOperationListener;
import org.spigot.swbfserver.maps.catalog.MapsCatalog;
import org.spigot.swbfserver.utils.SuperTable;

public class CatalogPane extends JPanel {
	private static final long serialVersionUID = 3782226460087537675L;
	public Object[] columnNames = new String[] { "Map code", "Map name", "Author", "Size", "Download&Install" };
	private JProgressBar progressBar;
	private JLabel lbldone;
	private JLabel lblop;
	private CatalogHandler handler;
	private SuperTable table;
	private JButton btnNewButton;
	private JPanel panel_1;
	private JButton btnStop;

	protected void setPanelName() {
		setName("MAP CATALOG (Deprecated)");
	}

	/**
	 * Create the panel.
	 */
	public CatalogPane() {
		setPanelName();
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		add(scrollPane, BorderLayout.CENTER);

		table = new SuperTable(columnNames, new String[] { "30", "grow", "70", "80", "80" });
		scrollPane.setViewportView(table);

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(10, 100));
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new MigLayout("", "[][300][grow]", "[grow][][][]"));

		JLabel lblCurrentOperation = new JLabel("Current operation:");
		panel.add(lblCurrentOperation, "cell 0 0,alignx right");

		lblop = new JLabel("<IDLE>");
		panel.add(lblop, "cell 1 0");

		panel_1 = new JPanel() {
			private static final long serialVersionUID = -2822609081353921790L;
			Image logo = MAIN.ImageLoader("swbflogo.png");

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(logo, 0, 0, null);
			}
		};
		panel.add(panel_1, "cell 2 0 1 4,grow");

		JLabel lblProgress = new JLabel("Progress:");
		panel.add(lblProgress, "cell 0 1,alignx right");

		progressBar = new JProgressBar();
		panel.add(progressBar, "cell 1 1,growx");

		JLabel lblFinished = new JLabel("Finished:");
		panel.add(lblFinished, "cell 0 2,alignx right");

		lbldone = new JLabel("");
		panel.add(lbldone, "cell 1 2");

		btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				handler.refresh();
			}
		});
		
		btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MapsCatalog.stopDownload();
			}
		});
		panel.add(btnStop, "cell 0 3,grow");
		panel.add(btnNewButton, "cell 1 3,growx");
		handler = new CatalogHandler();
	}

	protected void refresh(CatalogOperationListener l) {
		MapsCatalog.getCatalogMaps(l);
	}

	public class CatalogHandler {

		public CatalogOperationListener getOperationListener() {
			return l;
		}

		private CatalogOperationListener l;
		private List<CatalogMap> data = new ArrayList<>();

		public List<CatalogMap> getData() {
			return data;
		}

		public void refresh() {
			table.clear();
			CatalogPane.this.refresh(l);
		}

		private CatalogHandler() {
			this.l = new CatalogOperationListener() {

				@Override
				public void operationInProgress(String operation) {
					setCurrentOperation(operation);
				}

				@Override
				public void lockData() {
					disableTable();
				}

				@Override
				public void unlockData() {
					enableTable();
					setCurrentOperation("<IDLE>");
					setProgressValue(0);
					setProgressText("");
				}

				@Override
				public void setData(List<CatalogMap> data) {
					CatalogHandler.this.data = data;
					int len = data.size();
					Object[][] q = new Object[len][];
					int i = 0;
					for (final CatalogMap m : data) {
						JComponent tb;
						if (m.isInstalled()) {
							tb = new JLabel("Already installed.");
						} else {
							final JButton b = new JButton("Install");
							b.addActionListener(new ActionListener() {

								@Override
								public void actionPerformed(ActionEvent arg0) {
									m.addInstallListener(new Runnable() {

										@Override
										public void run() {
											table.replace(b, new JLabel("Already installed"));
										}

									});
									m.get(CatalogHandler.this);
								}

							});
							tb = b;
						}
						q[i] = new Object[] { m.getCode(), m.getName(), m.getAuthor(), m.getSizeString(), tb };
						i++;
					}
					CatalogHandler.this.setData(q);
				}

			};
		}

		public void setData(final Object[][] data) {
			try {
				SwingUtilities.invokeAndWait(new Runnable() {
					@Override
					public void run() {
						table.setData(data);
					}

				});
			} catch (InvocationTargetException | InterruptedException e) {
			}
		}

		public void enableTable() {
			table.enableComponents(true);
			btnNewButton.setEnabled(true);
			setProgressValue(0);
			setProgressText("");
			setCurrentOperation("<IDLE>");
			setCancellable(false);
		}

		public void disableTable() {
			table.enableComponents(false);
			btnNewButton.setEnabled(false);
		}
		
		public void setCancellable(boolean c) {
			btnStop.setEnabled(c);
		}

		public void setProgressValue(int value) {
			progressBar.setValue(value);
		}

		public void setKnownSize(boolean b) {
			progressBar.setIndeterminate(!b);
		}
		
		public void setProgressText(String text) {
			lbldone.setText(text);
		}

		public void setCurrentOperation(String text) {
			lblop.setText(text);
		}

		public void setOperationFinished() {
			refresh();
		}
	}

}
