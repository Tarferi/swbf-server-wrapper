package org.spigot.swbfserver.GUI;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileFilter;

import org.spigot.swbfserver.servers.ServerManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.awt.Color;

public class MainPane extends JPanel {
	private static final long serialVersionUID = 6501646001266479956L;
	private JTextField textField;
	private JLabel lblInvalidPath;
	private JTextField textField_1;

	/**
	 * Create the panel.
	 */
	public MainPane() {
		setName("Server Settings");
		setLayout(new MigLayout("", "[][grow]", "[][][]"));

		JLabel lblServerPath = new JLabel("Server path:");
		add(lblServerPath, "cell 0 0,alignx trailing");

		textField = new JTextField();
		textField.setText(ServerManager.getServerPath());
		add(textField, "flowx,cell 1 0,growx");
		textField.setColumns(10);
		textField.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				validatee();
			}

		});
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String path = getGameChoosePath();
				if (path != null) {
					textField.setText(path);
					ServerManager.setServerPath(path);
					validatee();
				}
			}
		});
		add(btnBrowse, "cell 1 0");

		lblInvalidPath = new JLabel("Invalid path");
		lblInvalidPath.setForeground(Color.RED);
		lblInvalidPath.setVisible(false);
		add(lblInvalidPath, "cell 1 1");
		
		JLabel lblPrearguments = new JLabel("Pre-Arguments");
		add(lblPrearguments, "cell 0 2,alignx trailing");
		
		textField_1 = new JTextField(ServerManager.getPreArguments());
		textField_1.getDocument().addUndoableEditListener(new UndoableEditListener() {

			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				String s=textField_1.getText();
				ServerManager.setPreArguments(s);
			}

		});
		add(textField_1, "cell 1 2,growx");
		textField_1.setColumns(10);
		validatee();
	}

	private void validatee() {
		String f = textField.getText();
		File ff = new File(f);
		if (!ff.exists()) {
			lblInvalidPath.setVisible(true);
			return;
		}
		if (!ff.getName().equalsIgnoreCase("battlefront server.exe")) {
			lblInvalidPath.setVisible(true);
			return;
		}
		lblInvalidPath.setVisible(false);
		ServerManager.setServerPath(f);

	}

	private static final String getGameChoosePath() {
		JFileChooser fc = new JFileChooser();
		FileFilter[] filters = fc.getChoosableFileFilters();
		for (FileFilter filter : filters) {
			fc.removeChoosableFileFilter(filter);
		}
		fc.addChoosableFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().equalsIgnoreCase("battlefront server.exe");
			}

			@Override
			public String getDescription() {
				return "battlefront server.exe";
			}

		});
		int ret = fc.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			if (f != null) {
				if (f.exists()) {
					return f.getAbsolutePath();
				}
			}
		}
		return null;
	}
}
