package org.spigot.swbfserver.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Image;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.spigot.swbfserver.MAIN;
import org.spigot.swbfserver.servers.Server;
import org.spigot.swbfserver.servers.ServerManager;
import org.spigot.swbfserver.servers.ServerSettings;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainGUI extends JFrame {

	private static final long serialVersionUID = -758819844517350651L;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private serverGuiManager manager;
	private MainPanelTabbed tbp;
	private static final Image SWBFIcon = MAIN.ImageLoader("ico.png").getScaledInstance(60, 60, Image.SCALE_SMOOTH);
	private static final ImageIcon SWBFImageIcon = new ImageIcon(SWBFIcon);

	public static Image getSWBFIcon() {
		return SWBFIcon;
	}

	public static ImageIcon getIcon() {
		return SWBFImageIcon;
	}

	/**
	 * Launch the application.
	 */
	public MainGUI() {
		setTitle("SWBF Server Wrapper");
		try {
			EventQueue.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					construct();
					setVisible(true);
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Create the frame.
	 */
	private void construct() {
		manager = new serverGuiManager();
		// super.setIconImage(SWBFIcon);
		ServerManager.setMainGuiManager(manager);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnMain = new JMenu("Main");
		menuBar.add(mnMain);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		final JMenuItem mntmEnableDeprecatedCatalog = new JMenuItem("Enable deprecated Catalog");
		mntmEnableDeprecatedCatalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				manager.addDeprecatedCatalog();
				mntmEnableDeprecatedCatalog.setEnabled(false);
			}
		});
		mnMain.add(mntmEnableDeprecatedCatalog);
		mnMain.add(mntmExit);

		JMenu mnServers = new JMenu("Servers");
		menuBar.add(mnServers);

		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ServerSettings set = new ServerSettings();
				Server s = ServerManager.addServer(set);
				manager.addServer(s);
			}
		});
		mnServers.add(mntmNew);

		JMenuItem mntmRemoveCurrentServer = new JMenuItem("Remove current server");
		mntmRemoveCurrentServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Server s = getServerByTab(tabbedPane.getSelectedIndex());
				if (s != null) {
					ServerManager.removeServer(s);
				}
			}
		});
		mnServers.add(mntmRemoveCurrentServer);

		JMenuItem mntmReloadMapList = new JMenuItem("Reload map list");
		mntmReloadMapList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ServerManager.reloadMaps();
			}
		});
		mnServers.add(mntmReloadMapList);

		JMenuItem mntmStartAll = new JMenuItem("Start All");
		mntmStartAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ServerManager.startAllServers();
			}
		});
		mnServers.add(mntmStartAll);

		JMenuItem mntmStopAll = new JMenuItem("Stop All");
		mntmStopAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ServerManager.stoptAllServers();
			}
		});
		mnServers.add(mntmStopAll);

		JMenuItem mntmRestartAll = new JMenuItem("Restart All");
		mntmRestartAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ServerManager.restartAllServers();
			}
		});
		mnServers.add(mntmRestartAll);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		JMenuItem mntmForums = new JMenuItem("Forums");
		mntmForums.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("http://www.swbfgamers.com/"));
				} catch (IOException e1) {
				} catch (URISyntaxException e1) {
				}
			}
		});
		mnHelp.add(mntmForums);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				About.open();
			}
		});
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tbp = new MainPanelTabbed();
		tabbedPane.add(tbp);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
	}

	private final HashMap<Server, ServerPanel> servers = new HashMap<>();

	private Server getServerByTab(int selectedIndex) {
		synchronized (servers) {
			for (Entry<Server, ServerPanel> s : servers.entrySet()) {
				if (s.getKey().getTabIndex() == selectedIndex) {
					return s.getKey();
				}
			}
		}
		return null;
	}

	public final class serverGuiManager {

		private CatalogPane dct;

		private serverGuiManager() {

		}

		public void addMainPanel() {
			tbp.addMainPanel();
		}

		public void addDeprecatedCatalog() {
			if (dct == null) {
				dct = new CatalogPane();
				tbp.add(dct);
			}
		}

		public void addCatalogPanel() {
			tbp.addCatalogPanel();
		}
		
		public void addMapManagerPanel() {
			tbp.addMapManagerPanel();
		}

		public void addServer(Server s) {
			synchronized (servers) {
				ServerPanel panel = new ServerPanel(s);
				panel.setName(s.getSettings().getServerName());
				tabbedPane.add(panel);
				servers.put(s, panel);
				updateServers();
			}
		}

		private void updateServers() {
			synchronized (servers) {
				Set<Entry<Server, ServerPanel>> se = servers.entrySet();
				for (Entry<Server, ServerPanel> s : se) {
					int max = tabbedPane.getTabCount();
					Component com = s.getValue();
					for (int i = 0; i < max; i++) {
						Component cmp = tabbedPane.getComponentAt(i);
						if (cmp == com) {
							s.getKey().setTabIndex(i);
							break;
						}
					}
				}
			}
		}

		public void updateServerName(Server s) {
			if (s.getTabIndex() >= 0) {
				tabbedPane.setTitleAt(s.getTabIndex(), s.getSettings().getServerName());
			}
		}

		public void removeServer(Server s) {
			synchronized (servers) {
				if (servers.containsKey(s)) {
					ServerPanel panel = servers.remove(s);
					tabbedPane.remove(panel);
				}
				updateServers();
			}
		}
	}

}
