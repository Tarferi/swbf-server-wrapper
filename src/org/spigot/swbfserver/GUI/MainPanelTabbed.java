package org.spigot.swbfserver.GUI;

import javax.swing.JTabbedPane;

public class MainPanelTabbed extends JTabbedPane {

	private static final long serialVersionUID = -5100462717012847147L;

	public MainPanelTabbed() {
		super.setName("MAIN");
	}

	public void addMainPanel() {
		super.add(new MainPane());
	}

	public void addCatalogPanel() {
		super.add(new CatalogPaneNew());
	}

	public void addDeprecatedCatalogPanel() {
		super.add(new CatalogPane());
	}

	public void addMapManagerPanel() {
		super.add(new MapManagerPane());
	}
}
