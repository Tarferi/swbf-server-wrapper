package org.spigot.swbfserver.GUI;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import java.awt.Dimension;

import javax.swing.JLabel;



import org.spigot.swbfserver.MAIN;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class About extends JDialog {
	public About() {
	}

	private static final long serialVersionUID = 3886753530127814930L;
	private static About w;
	private final JPanel contentPanel = new JPanel();

	public static final void open() {
		if (w == null) {
			w = new About();
			w.construct();
			w.setVisible(true);
		}
		w.requestFocus();
	}

	private void construct() {
		setResizable(false);
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				w = null;
			}
		});
		setBounds(100, 100, 450, 215);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		JPanel panel5 = new JPanel();
		contentPanel.add(panel5, BorderLayout.SOUTH);
		panel5.setLayout(new MigLayout("", "[grow,right]", "[]"));
		JButton btnClose = new JButton("Close");
		btnClose.setPreferredSize(new Dimension(75, 30));
		panel5.add(btnClose, "cell 0 0,alignx center");
		btnClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispatchEvent(new WindowEvent(About.this, WindowEvent.WINDOW_CLOSING));
			}

		});
		JPanel panel6 = new JPanel();
		contentPanel.add(panel6, BorderLayout.CENTER);
		panel6.setLayout(new MigLayout("", "[120][269.00]", "[120]"));
		JPanel imgpane = new JPanel() {
			private static final long serialVersionUID = -4828694907770737377L;

			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(MainGUI.getSWBFIcon(), 0, 0, super.getWidth(), super.getHeight(), null);
			}
		};
		panel6.add(imgpane, "cell 0 0,grow");
		JPanel panel_1 = new JPanel();
		panel6.add(panel_1, "cell 1 0,grow");
		panel_1.setLayout(new MigLayout("", "[]", "[][][]"));
		JLabel lblNewLabel = new JLabel("SWBF Server wrapper");
		panel_1.add(lblNewLabel, "cell 0 0");
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 28));
		lblNewLabel.setForeground(Color.BLUE);
		JLabel lblBysonic = new JLabel("made by");
		panel_1.add(lblBysonic, "flowx,cell 0 1,aligny center");
		JLabel lblSonic = new JLabel("{212} Sonic");
		lblSonic.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSonic.setForeground(new Color(72, 209, 204));
		panel_1.add(lblSonic, "cell 0 1");
		JLabel lblVersin = new JLabel("version");
		panel_1.add(lblVersin, "flowx,cell 0 2");
		JLabel label = new JLabel(MAIN.version);
		label.setForeground(new Color(154, 205, 50));
		label.setEnabled(true);
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_1.add(label, "cell 0 2,aligny center");
	}
}
