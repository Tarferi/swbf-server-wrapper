package org.spigot.swbfserver.GUI;

import org.spigot.swbfserver.maps.catalog.CatalogOperationListener;
import org.spigot.swbfserver.maps.catalog.MapsCatalog;

public class CatalogPaneNew extends CatalogPane {

	private static final long serialVersionUID = 6154628637251279028L;
	
	@Override
	protected void setPanelName() {
		setName("MAP CATALOG");
	}
	
	@Override
	protected void refresh(CatalogOperationListener l) {
		MapsCatalog.getCatalogMapsNew(l);
	}
}
