package org.spigot.swbfserver.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.spigot.swbfserver.maps.Map;
import org.spigot.swbfserver.servers.ServerManager;
import org.spigot.swbfserver.utils.SuperTable;

import net.miginfocom.swing.MigLayout;

public class MapManagerPane extends JPanel {

	private static final long serialVersionUID = 6449611027903480765L;
	private SuperTable table;
	private MapManager manager;

	public MapManagerPane() {
		construct();
	}

	private void construct() {
		setName("Map Manager");
		setLayout(new MigLayout("", "[grow]", "[260.00,grow][120]"));

		table = new SuperTable(new Object[] { "Map code", "Map name", "Active", "Enable/Disable" }, new String[] { "", "grow", "", "" });

		JScrollPane scroll = new JScrollPane(table);
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		add(scroll, "cell 0 0,grow");

		JPanel panel = new JPanel();
		add(panel, "cell 0 1,grow");
		panel.setLayout(new MigLayout("", "[]", "[][][]"));

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				manager.refresh();
			}
		});
		panel.add(btnRefresh, "cell 0 0,grow");

		JButton btnDisableAll = new JButton("Disable All");
		btnDisableAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manager.disableAll();
			}
		});
		panel.add(btnDisableAll, "cell 0 1,grow");

		JButton btnEnableAll = new JButton("Enable All");
		btnEnableAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manager.enableAll();
			}
		});
		panel.add(btnEnableAll, "cell 0 2,grow");
		manager = new MapManager();
		manager.refresh();
	}

	public class MapManager {

		public void refresh() {
			List<Map> enabled = ServerManager.getEnabledMapList();
			List<Map> disabled = ServerManager.getDisabledMapList();
			setData(enabled, disabled);
			table.repaint();
		}

		public void enableAll() {
			List<Map> disbled = ServerManager.getDisabledMapList();
			for (Map m : disbled) {
				ServerManager.enableMap(m);
			}
			refresh();
		}

		public void disableAll() {
			List<Map> enabled = ServerManager.getEnabledMapList();
			for (Map m : enabled) {
				ServerManager.disableMap(m);
			}
			refresh();
		}

		private void setData(List<Map> enabled, List<Map> disabled) {
			int len1 = enabled.size();
			int len2 = disabled.size();
			Object[][] o = new Object[len1 + len2][];
			int i = 0;
			for (final Map m : enabled) {
				JButton b = new JButton("Disable");
				b.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						ServerManager.disableMap(m);
						refresh();
					}
				});
				JCheckBox c = new JCheckBox();
				c.setEnabled(false);
				c.setSelected(true);
				o[i] = new Object[] { m.getPureMapCode(), m.toStringNoEra(), c, b };
				i++;
			}

			for (final Map m : disabled) {
				JButton b = new JButton("Enable");
				b.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						ServerManager.enableMap(m);
						refresh();
					}
				});
				JCheckBox c = new JCheckBox();
				c.setEnabled(false);
				c.setSelected(false);
				o[i] = new Object[] { m.getPureMapCode(), m.toString(), c, b };
				i++;
			}
			table.clear();
			table.setData(o);
		}
	}
}
