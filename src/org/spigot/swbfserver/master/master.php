<?php
if (! isset ( $_GET ["action"] )) {
	die ( "FAIL 1" );
}
$action = $_GET ["action"];

if ($action == "GET") {
	$db = new SDB ();
	$data = $db->getData ();
	die ( $data );
}

if ($action == "POST") {
	
	if (! isset ( $_GET ["data"] )) {
		die ( "FAIL 2" );
	}
	
	$data = $_GET ["data"];
	
	$data = base64_decode ( $data );
	if (! $data) {
		die ( "FAIL 3" );
	}
	$delim1 = chr ( 1 );
	$delim2 = chr ( 2 );
	$delim3 = chr ( 3 );
	$delim4 = chr ( 4 );
	
	$servers = array ();
	$serverss = explode ( $delim4, $data );
	foreach ( $serverss as $server ) {
		$q = explode ( $delim3, $server );
		$servername = $q [0];
		$m = explode ( $delim2, $q [1] );
		$maps = array ();
		foreach ( $m as $map ) {
			$q = explode ( $delim1, $map );
			$maps [] = new Map ( $q [0], $q [1], $q [2] );
		}
		$servers [] = new Server ( $servername, $maps );
	}
	
	$db = new SDB ();
	$db->refreshData ( $_SERVER ["REMOTE_ADDR"], time (), $servers );
	file_put_contents("backup.txt",json_encode($servers));
	$db->close ();
	die ( "OK" );
}
DIE ( "FAIL 4" );

class Map {

	public function __construct($code, $name, $url) {
		$this->Code = $code;
		$this->MapName = $name;
		$this->getURL = $url;
	}
}

class Server {

	public function __construct($servername, $maps) {
		$this->serverName = $servername;
		$this->Maps = $maps;
	}
}

class SDB extends SQLite3 {

	function getData() {
		$this->removeOldData ();
		$res = $this->query ( "SELECT * from servers" );
		$data = array ();
		while ( $row = $res->fetchArray ( SQLITE3_ASSOC ) ) {
			$data [] = $row ["servername"] . chr ( 3 ) . $row ["ip"] . chr ( 3 ) . $row ["maps"];
		}
		return implode ( chr ( 2 ), $data );
	}

	function refreshData($ip, $time, $servers) {
		$this->removeOldData ();
		foreach ( $servers as $server ) {
			$maps = $server->Maps;
			$d = array ();
			foreach ( $maps as $map ) {
				$d [] = $map->Code . chr ( 1 ) . $map->MapName . chr ( 1 ) . $map->getURL;
			}
			$maps = implode ( chr ( 6 ), $d );
			$this->exec ( "DELETE FROM servers WHERE ip='" . $ip . "' AND servername='" . $server->serverName . "'" );
			$this->exec ( "INSERT INTO servers ('ip','last_update','servername','maps') VALUES ('" . $ip . "','" . time () . "','" . $server->serverName . "','" . $maps . "')" );
		}
	}

	function removeOldData() {
		$this->exec ( "DELETE FROM servers WHERE last_update<'" . (time () - $this->cache_delay) . "'" );
	}

	function __construct() {
		$this->cache_delay = 20 * 60;
		$this->open ( 'servers.db' );
	}
}
?>