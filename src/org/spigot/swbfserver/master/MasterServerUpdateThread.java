package org.spigot.swbfserver.master;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.spigot.swbfserver.servers.Server;
import org.spigot.swbfserver.servers.ServerManager;
import org.spigot.swbfserver.servers.ServerMap;
import org.spigot.swbfserver.utils.Base64;
import org.spigot.swbfserver.utils.URLRetriever;

public class MasterServerUpdateThread {

	private static final String Delim1 = new String(new byte[] { 1 });
	private static final String Delim2 = new String(new byte[] { 2 });
	private static final String Delim3 = new String(new byte[] { 3 });
	private static final String Delim4 = new String(new byte[] { 4 });

	private static Thread thread;
	private static final int delay = 10 * 60;
	private static final String masterURL = "http://swbf.ithief.net/master.php";
	private static final Object syncer = new Object();

	public static void addServer(Server s) {
		data.put(s, new MasterData(s));
		update();
	}

	public static void removeServer(Server s) {
		data.remove(s);
		update();
	}

	private static HashMap<Server, MasterData> data = new HashMap<>();

	private static class MasterData {
		private String serverName;
		private String maps;

		private MasterData(Server s) {
			serverName = s.getSettings().getServerName();
			List<ServerMap> dm = s.getRightMaps();
			StringBuilder sb = new StringBuilder();
			for (ServerMap m : dm) {
				ServerManager.getMapByCode(m.getMapCode());
				String origin = m.getURLOrigin();
				sb.append(m.getPureMapCode() + Delim1 + m.toString() + Delim1 + origin + Delim2);
			}
			if (sb.length() > 0) {
				sb.setLength(sb.length() - 1);
			}
			maps = sb.toString();
		}

		private String getData() {
			return serverName + Delim3 + maps;
		}
	}

	static {
		construct();
	}

	private static void construct() {
		thread = new Thread() {
			@Override
			public void run() {
				synchronized (syncer) {
					while (true) {
						try {
							handle();
							syncer.wait(delay * 1000);
						} catch (InterruptedException e) {
						}
					}
				}
			}
		};
		thread.start();
	}

	public static void update() {
		synchronized (syncer) {
			syncer.notify();
		}
	}

	@SuppressWarnings("deprecation")
	public static void stop() {
		if (thread != null) {
			thread.stop();
		}

	}

	private static void handle() {
		StringBuilder sb = new StringBuilder();
		for (Entry<Server, MasterData> d : data.entrySet()) {
			sb.append(d.getValue().getData() + Delim4);
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		String data = Base64.getEncoder().encodeToString(sb.toString().getBytes());
		if (!data.isEmpty()) {
			String url = masterURL + "?action=POST&data=" + data;
			String resp = URLRetriever.getURLContent(url);
			if (!resp.equals("OK")) {
				System.err.println("Failed to report to master server: " + resp);
			}
		}
	}
}
