package org.spigot.swbfserver.extractorAdapter;

import java.awt.Desktop;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JOptionPane;

import org.spigot.swbfserver.MAIN;

public class ExtractorAdapter {
	private static Constructor<?> constructor;
	private static Method extractM;
	private static Method getFileM;
	private static final File f = new File(MAIN.getCurrentFolder().getAbsolutePath() + "/libs/Extractor.jar");

	public static boolean loadLibrary() {
		return loadLibrary(false);
	}

	@SuppressWarnings("resource")
	public static boolean loadLibrary(boolean b) {
		try {
			URLClassLoader cls = new URLClassLoader(new URL[] { f.toURI().toURL() }, System.class.getClassLoader());
			cls.loadClass("net.ithief.extractor.Extractor");
			Class<?> ec = cls.loadClass("net.ithief.extractor.ExternalExtract");
			constructor = ec.getConstructor(String.class);
			extractM = ec.getMethod("extract", String.class, String.class);
			getFileM = ec.getMethod("getFile");
			return true;
		} catch (Throwable e) {
			System.err.println("Failed to load file Extractor.jar");
			if (b) {
				noLibrary();
				return false;
			} else {
				return new LibGetGUI().commit();
			}
		}
	}

	private static void noLibrary() {
		int opt = JOptionPane.showConfirmDialog(null, "Required Extractor library is missing or corrupted.\nWould you like to navigate to download page?", "Library missing", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
		if (opt == JOptionPane.YES_OPTION) {
			try {
				Desktop.getDesktop().browse(new URI(MAIN.getLibraryURI()));
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		System.exit(1);
	}

	private Object instance;

	public ExtractorAdapter(String look) {
		try {
			instance = constructor.newInstance(look);
		} catch (Throwable e) {
			e.printStackTrace();
			noLibrary();
		}
	}

	public boolean extract(String file, String dir) throws SevenZipException {
		try {
			return (boolean) extractM.invoke(instance, file, dir);
		} catch (Throwable e) {
			throw new SevenZipException(e.getMessage());
		}
	}

	public File getFile() {
		try {
			return (File) getFileM.invoke(instance);
		} catch (Throwable e) {
			return null;
		}
	}

	public class SevenZipException extends Exception {
		private static final long serialVersionUID = -2562106513354383482L;

		private SevenZipException(String msg) {
			super(msg);
		}
	}

	public static File getLibraryFile() {
		return f;
	}
}
