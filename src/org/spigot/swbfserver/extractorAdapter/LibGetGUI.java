package org.spigot.swbfserver.extractorAdapter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JProgressBar;
import javax.swing.JLabel;

import org.spigot.swbfserver.MAIN;
import org.spigot.swbfserver.utils.IOUtils;
import org.spigot.swbfserver.utils.IOUtils.ProgressListener;
import org.spigot.swbfserver.utils.URLRetriever;
import org.spigot.swbfserver.utils.URLRetriever.MaxLengthAvailableListener;

public class LibGetGUI extends JFrame {

	private static final long serialVersionUID = 4551146530511711884L;
	private JPanel contentPane;
	private int size = -1;
	private URLRetriever retriever;
	private InputStream input;
	protected boolean fail = false;
	private JProgressBar progressBar;
	private JLabel label;
	private String rsize;

	public LibGetGUI() {
		construct();
		setVisible(true);
	}

	public boolean commit() {
		retriever = new URLRetriever();
		retriever.setURL(MAIN.getLibraryURI());
		input = retriever.commitCurrentGetInput(new MaxLengthAvailableListener() {

			@Override
			public void maxLength(int length) {
				size = length;
				updateSize();
			}

		});
		OutputStream out = null;
		try {
			ExtractorAdapter.getLibraryFile().getParentFile().mkdirs();
			out = new FileOutputStream(ExtractorAdapter.getLibraryFile().getAbsolutePath());
		} catch (FileNotFoundException e1) {
			return false;
		}
		IOUtils.copy(input, out, new ProgressListener() {

			@Override
			public void setValue(long bytes) {
				sizeSize((int) bytes);
			}

			@Override
			public void setMaximum(long maximum) {
				size = (int) maximum;
				updateSize();
			}

			@Override
			public void stopped() {
				fail = true;
			}

			@Override
			public void fail(Throwable e) {
				fail = true;
			}

		}).commit();
		try {
			out.close();
		} catch (Exception e1) {
		}
		retriever.close();
		if (fail) {
			JOptionPane.showMessageDialog(null, "Process failed.\nApplication will now exit.", "Fail", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} else {
			dispose();
			return ExtractorAdapter.loadLibrary(true);
		}
		return false;
	}

	private String readableFileSize(long size) {
		if (size <= 0) {
			return "0";
		}
		final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}

	protected void sizeSize(int bytes) {
		progressBar.setValue((bytes * 100) / size);
		label.setText(size == -1 ? readableFileSize(bytes) : readableFileSize(bytes) + "/" + rsize);
	}

	protected void updateSize() {
		rsize = readableFileSize(size);
		progressBar.setIndeterminate(size == -1);
	}

	private void construct() {
		setTitle("Downloading libraries");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 297, 92);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow]", "[][]"));

		progressBar = new JProgressBar();
		contentPane.add(progressBar, "cell 0 0 2 1,grow");

		JLabel lblFinished = new JLabel("Finished:");
		contentPane.add(lblFinished, "cell 0 1");

		label = new JLabel("Pending...");
		contentPane.add(label, "cell 1 1");
	}
}
