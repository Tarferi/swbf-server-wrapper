package org.spigot.swbfserver.maps;

public enum Era {
	GCW("GCW"), CW("CW");
	private String n;

	private Era(String n) {
		this.n = n;
	}

	@Override
	public String toString() {
		return n;
	}
}
