package org.spigot.swbfserver.maps;

import java.io.Serializable;

import org.spigot.swbfserver.exceptions.InvalidMapException;
import org.spigot.swbfserver.servers.ServerManager;

public class Map implements Serializable {

	private static final long serialVersionUID = 6181077601346060613L;

	private String code, planet, battle;
	private Era era = Era.GCW;
	private int rein1, rein2;

	private String lcode;

	private String path;

	public void setFolder(String path) {
		this.path = path;
	}

	public String getURLOrigin() {
		if (path != null) {
			return ServerManager.getMapOrigin(this);
		}
		return null;
	}

	public String getPath() {
		return path;
	}

	public Map(String code, int rein1, int rein2) {
		this.code = code;
		this.rein1 = rein1;
		this.rein2 = rein2;
		resolve();
		resolveLCode();
	}

	private void resolveLCode() {
		lcode = code.substring(0, code.length() - 1);
	}

	public Map(String code, int rein1, int rein2, String planet, String battle) {
		this.code = code;
		this.rein1 = rein1;
		this.rein2 = rein2;
		this.planet = planet;
		this.battle = battle;
		resolveEra();
		resolveLCode();
	}

	@Override
	public String toString() {
		if (planet == null) {
			return era + ": " + battle;
		} else {
			return era + ": " + planet + ": " + battle;
		}
	}

	public String toStringNoEra() {
		if (planet == null) {
			return battle;
		} else {
			return planet + ": " + battle;
		}
	}

	private void resolveEra() {
		String last = code.substring(code.length() - 1, code.length());
		era = Era.GCW;
		switch (last) {
			case "r":
			case "c":
				era = Era.CW;
		}
	}

	private void resolve() {
		resolveEra();
		String map = code.substring(0, code.length() - 1);
		String m = "Unknown";
		String p = "Unknown";
		switch (map) {
			case "bes2":
				p = "Bespin";
				m = "Cloud City";
			break;
			case "bes1":
				p = "Bespin";
				m = "Platforms";
			break;
			case "end1":
				p = "Endor";
				m = "Bunker";
			break;
			case "geo1":
				p = "Geonosis";
				m = "Spire";
			break;
			case "hot1":
				p = "Hoth";
				m = "Echo Base";
			break;
			case "kam1":
				p = "Kamino";
				m = "Topica City";
			break;
			case "kam2":
				p = "Kamino";
				m = "Nuricoca City";
			break;
			case "kas2":
				p = "Kashyyyk";
				m = "Docks";
			break;
			case "kas1":
				p = "Kashyyyk";
				m = "Islands";
			break;
			case "nab1":
				p = "Naboo";
				m = "Plains";
			break;
			case "nab2":
				p = "Naboo";
				m = "Theed";
			break;
			case "rhn1":
				p = "Rhen Var";
				m = "Harbour";
			break;
			case "rhn2":
				p = "Rhen Var";
				m = "Citadel";
			break;
			case "tat1":
				p = "Tatooine";
				m = "Dune Sea";
			break;
			case "tat2":
				p = "Tatooine";
				m = "Mos Eisley";
			break;
			case "tat3":
				p = "Tatooine";
				m = "Jabba's Place";
			break;
			case "yav1":
				p = "Yavin 4";
				m = "Temple";
			break;
			case "yav2":
				p = "Yavin 4";
				m = "Arena";
			break;
			case "cor2":
			case "corus2":
				p = "Coruscant";
				m = "Streets";
			break;
			case "cor1":
				p = "Coruscant";
				m = "Temple";
			break;
			case "dag1":
				p = "Dagobah";
				m = "Swamps";
			break;
			case "dan1":
				p = "Dantooine";
				m = "Dust";
			break;
			case "edd1":
				p = "Unknown";
				m = "Eddie's Kastel";
			break;
			case "fel1":
				p = "Felucia";
				m = "Woods";
			break;
			case "hot2":
				p = "Hoth";
				m = "Cave";
			break;
			case "koh1":
				p = "Kohlma";
				m = "Moon of the dead";
			break;
			case "mus1":
				p = "Mustafar";
				m = "Refinery";
			break;
			case "nar1":
				p = "Nar Shaddaa";
				m = "The roof";
			break;
			case "uta1":
				p = "Utapau";
				m = "Sinkhole";
			break;
		}
		planet = p;
		battle = m;
	}

	public String getMapCode() {
		return code;
	}

	public String getPureMapCode() {
		return lcode;
	}

	public Era getEra() {
		return era;
	}

	public void validate() throws InvalidMapException {

	}

	public int getReinforcements1() {
		return rein1;
	}

	public int getReinforcements2() {
		return rein2;
	}

	protected void setRein1(int rein12) {
		rein1 = rein12;
	}

	protected void setRein2(int rein12) {
		rein2 = rein12;
	}

	public String getPlanet() {
		return planet;
	}

	public String getBattle() {
		return battle;
	}
}