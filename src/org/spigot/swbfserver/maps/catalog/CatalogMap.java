package org.spigot.swbfserver.maps.catalog;

import java.io.File;
import java.nio.file.Files;
import java.text.DecimalFormat;

import org.spigot.swbfserver.GUI.CatalogPane.CatalogHandler;
import org.spigot.swbfserver.extractorAdapter.ExtractorAdapter;
import org.spigot.swbfserver.servers.ServerManager;
import org.spigot.swbfserver.utils.IOUtils.ProgressListener;

public class CatalogMap {

	private String code;
	private String name;
	private String url;
	private long size;
	private String ssize;
	private String author;
	private boolean isinstalled;
	private Runnable r;

	public void addInstallListener(Runnable r) {
		this.r = r;
	}

	protected CatalogMap(String code, String name, long size, String strsize, String url, String author) {
		this.code = code;
		this.name = name;
		this.size = size;
		this.url = url;
		this.author = author;
		this.ssize = strsize;
		isinstalled = false;
		if (!code.equals("???")) {
			isinstalled = ServerManager.getMapByPureCode(code);
		}
	}

	public void setInstalled() {
		isinstalled = true;
		if (r != null) {
			r.run();
		}
	}

	public boolean isInstalled() {
		return isinstalled;
	}

	public String getURL() {
		return url;
	}

	public String getSizeString() {
		return ssize;
	}

	public long getSize() {
		return size;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public void get(final CatalogHandler h) {
		h.disableTable();
		h.setKnownSize(false);
		try {
			MapsCatalog.getMap(this, new ProgressListener() {
				long max = 0;
				boolean single = true;
				String maxx = readableFileSize(0);

				private String readableFileSize(long size) {
					if (size <= 0) {
						return "0";
					}
					final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
					int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
					return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
				}

				@Override
				public void setMaximum(long m) {
					max = m;
					maxx = readableFileSize(max);
					single = max == 0 || max == -1;
					h.setKnownSize(!single);
				}

				@Override
				public void setValue(long v) {
					if (single) {
						h.setProgressText(readableFileSize(v));
					} else {
						h.setProgressValue((int) (v * 100 / max));
						h.setProgressText(readableFileSize(v) + "/" + maxx);
					}
				}

				@Override
				public void stopped() {
					h.enableTable();
					h.setProgressText("Download stopped!");
				}

				@Override
				public void fail(Throwable e) {
					h.enableTable();
					h.setProgressText("Download failed!");
					e.printStackTrace();

				}

			}, h.getOperationListener(), new CatalogOperationHandler() {

				@Override
				public void operationFailed() {
					h.enableTable();
					h.setProgressText("Download failed!");
				}

				@Override
				public void operationFinished(File f, String name) {
					h.setProgressText("Download finished!");
					h.setKnownSize(false);
					File tf = null;
					try {
						String targetdir = ServerManager.getServerFolderPath() + "/AddOn/";
						String targetfile = targetdir + "/" + name;
						tf = new File(targetfile);
						Files.copy(f.toPath(), tf.toPath());
						ExtractorAdapter e = new ExtractorAdapter("addme.script");
						if (!e.extract(targetfile, targetdir)) {
							h.enableTable();
							h.setProgressText("File extract failed.");
							tf.delete();
							return;
						}
						ServerManager.setMapOrigin(e.getFile().getParent(), url);
						h.enableTable();
						h.setProgressText("Map install finished!");
						h.setKnownSize(true);
						ServerManager.reloadMaps();
						h.setOperationFinished();
					} catch (Throwable e) {
						h.setKnownSize(true);
						h.enableTable();
						e.printStackTrace();
						h.setProgressText("Map install failed (" + e.getMessage() + ")!");
						if (tf != null) {
							tf.delete();
						}
					}
				}

				@Override
				public void setCancellable(boolean c) {
					h.setCancellable(c);
				}

			});
		} catch (Exception e) {
			e.printStackTrace();
			h.enableTable();
			h.setKnownSize(true);
			h.setProgressText("Map install failed (" + e.getMessage() + ")!");
		}
	}

	public interface CatalogOperationHandler {

		public void setCancellable(boolean c);
		
		public void operationFailed();

		public void operationFinished(File f, String name);

	}
}
