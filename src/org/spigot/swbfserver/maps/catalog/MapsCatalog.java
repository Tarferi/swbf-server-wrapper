package org.spigot.swbfserver.maps.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.spigot.swbfserver.MAIN;
import org.spigot.swbfserver.maps.catalog.CatalogMap.CatalogOperationHandler;
import org.spigot.swbfserver.utils.IOUtils;
import org.spigot.swbfserver.utils.IOUtils.CopyOperation;
import org.spigot.swbfserver.utils.IOUtils.ProgressListener;
import org.spigot.swbfserver.utils.IntegerUtils;
import org.spigot.swbfserver.utils.ShaHasher;
import org.spigot.swbfserver.utils.URLRetriever;
import org.spigot.swbfserver.utils.URLRetriever.MaxLengthAvailableListener;

public class MapsCatalog {

	private static final void sort(List<CatalogMap> maps) {
		Comparator<CatalogMap> comp = new Comparator<CatalogMap>() {

			@Override
			public int compare(CatalogMap arg0, CatalogMap arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}

		};
		Collections.sort(maps, comp);
	}

	public static void getCatalogMaps(final CatalogOperationListener l) {
		l.lockData();
		new Thread() {
			@Override
			public void run() {
				setName("Catalog worker");
				new MapsCatalog(l);
			}
		}.start();
	}

	public static void getCatalogMapsNew(final CatalogOperationListener l) {
		l.lockData();
		new Thread() {
			@Override
			public void run() {
				setName("Catalog worker");
				new MapsCatalog(l, true);
			}
		}.start();
	}

	protected static void getMap(final CatalogMap m, final ProgressListener h, final CatalogOperationListener l, final CatalogOperationHandler ch) {
		l.lockData();
		new Thread() {
			@Override
			public void run() {
				setName("Catalog worker");
				try {
					new MapsCatalog(m, h, l, ch);
				} catch (Throwable e) {
					l.unlockData();
					l.operationInProgress("Operation failed");
				}
			}
		}.start();
	}

	private static CopyOperation op;

	private MapsCatalog(final CatalogOperationListener l) {
		try {
			List<CatalogMap> cm = new ArrayList<>();
			getList(l, cm);
		} catch (Throwable e) {
			l.unlockData();
			l.operationInProgress("Operation failed");
		}
	}

	private MapsCatalog(final CatalogOperationListener l, boolean neww) {
		try {
			List<CatalogMap> cm = new ArrayList<>();
			getList2(l, cm);
		} catch (Throwable e) {
			l.unlockData();
			l.operationInProgress("Operation failed");
		}
	}

	public MapsCatalog(CatalogMap m, final ProgressListener lh, final CatalogOperationListener l, CatalogOperationHandler ch) {
		retriever = new URLRetriever();
		InputStream input = null;
		FileOutputStream out = null;
		try {
			login(l);
			downloadDetails d = getMapFileName(m);
			l.operationInProgress("Downloading file");
			retriever.setGet();
			retriever.setURL(d.getURL());
			lh.setMaximum(m.getSize());
			input = retriever.commitCurrentGetInput(new MaxLengthAvailableListener() {

				@Override
				public void maxLength(int length) {
					lh.setMaximum(length);
				}
				
			});
			File tmp = MAIN.getTempFile();
			out = new FileOutputStream(tmp);
			op=IOUtils.copy(input, out, lh);
			ch.setCancellable(true);
			op.commit();
			ch.setCancellable(false);
			l.operationInProgress("Extracting file");
			ch.operationFinished(tmp, d.getFileName());
		} catch (Throwable e) {
			l.operationInProgress("Error happened: " + e.getMessage());
			e.printStackTrace();
			ch.operationFailed();
		}
		IOUtils.closeQuietly(input);
		IOUtils.closeQuietly(out);
		retriever.close();
	}

	public static void stopDownload() {
		try {
			op.stop();
		} catch (Throwable e) {
		}
	}
	
	private URLRetriever retriever;

	private static final String delim1 = new String(new byte[] { 1 });

	private void getList2(CatalogOperationListener l, List<CatalogMap> cm) {
		l.operationInProgress("Getting map list");
		String resp = URLRetriever.getURLContent("http://swbf.ithief.net/code.txt");
		l.operationInProgress("Parsing map list");
		if (resp != null) {
			resp = resp.replaceAll("\r", "");
			String[] r = resp.split("\n");
			String last = "";
			for (String a : r) {
				String[] q = a.split(delim1);
				if (q.length == 11) {
					if (!q[1].equals("TRASH")) {
						String code = q[0];
						String lcode = code.substring(0, code.length() - 1);
						if (!lcode.equals(last)) {
							last = lcode;
							code = lcode;
							String author = q[6];
							String name = q[7];
							long size = IntegerUtils.parseIntOrDefault(q[8], 0);
							String sizestr = q[9];
							String url = q[10];
							CatalogMap m = new CatalogMap(code, name, size, sizestr, url, author);
							cm.add(m);
						}
					}
				}
			}
		}
		sort(cm);
		l.setData(cm);
		l.unlockData();
	}

	private void getList(CatalogOperationListener l, List<CatalogMap> cm) {
		retriever = new URLRetriever();
		try {
			login(l);
			getMapList(l, cm);
			sort(cm);
			l.setData(cm);
			l.unlockData();
		} catch (Throwable e) {
			l.unlockData();
			l.operationInProgress("Error happened: " + e.getMessage());
			e.printStackTrace();
		}
		retriever.close();
	}

	private static final String token_Login1 = "nsubmit=\"hashLoginPassword(this, '";
	private static final String username = "MapCatalogBot3";
	private static final String password = "123456789";

	private void login(CatalogOperationListener l) throws CatalogException {
		l.operationInProgress("Retrieving login token");
		retriever.setURL("http://www.swbfgamers.com/index.php?action=login");
		retriever.setGet();
		retriever.commitCurrent();
		String lastpage = retriever.getLastPageTitle();
		if (!lastpage.equals("Login")) {
			if (lastpage.equals("SWBFGamers - Star Wars Battlefront Maps")) {
				return;
			}
			throw new CatalogException("Failed to fetch login page (" + lastpage + ").");
		}
		String resp = retriever.getResponse();
		int index1 = resp.indexOf(token_Login1);
		if (index1 == -1) {
			throw new CatalogException("Failed to get login token.");
		}
		index1 += token_Login1.length();
		int index2 = resp.indexOf("'", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to get login token end.");
		}
		String session = resp.substring(index1, index2);
		l.operationInProgress("Logging in");
		String passhash;
		try {
			passhash = ShaHasher.getHash(ShaHasher.getHash(username.toLowerCase() + password) + session);
		} catch (Exception e) {
			throw new CatalogException("Failed to calculate password hash.");
		}
		retriever.setURL("http://www.swbfgamers.com/index.php?action=login2");
		retriever.setPost();
		retriever.addPostField("user", username);
		retriever.addPostField("password", "");
		retriever.addPostField("cookielength", "60");
		retriever.addPostField("hash_passwrd", passhash);
		retriever.addPostField("bb2_screener_", System.currentTimeMillis() / 1000 + " 127.0.0.1");
		retriever.commitCurrent();
		retriever.getResponse();
	}

	private static final String token_MAPS = "Pages:";
	private static final String token_MAPSEND = "</td>";

	private static final String token_MAPS2 = "<a class=\"navPages\" href=\"";
	private static final String token_MAPS2END = "</a>";

	private void getMapList(CatalogOperationListener l, List<CatalogMap> maps) throws CatalogException {
		retriever.setURL("http://www.swbfgamers.com/index.php?action=downloads;cat=1");
		retriever.commitCurrent();
		if (retriever.getLastPageTitle() == null) {
			throw new CatalogException("Failed to receive map list.");
		}
		if (!retriever.getLastPageTitle().equals("SWBFGamers - Star Wars Battlefront Maps")) {
			throw new CatalogException("Failed to receive map list.");
		}
		l.operationInProgress("Parsing map list");
		String resp = retriever.getResponse();
		int index1 = resp.indexOf(token_MAPS);
		if (index1 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		index1 += token_MAPS.length();
		int index2 = resp.indexOf(token_MAPSEND, index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to parse map list end.");
		}
		index2 += token_MAPSEND.length();
		String data = resp.substring(index1, index2).trim();
		int first = 1;
		int len = 0;
		int last = 0;
		index1 = data.lastIndexOf(token_MAPS2);
		if (index1 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		index1 += token_MAPS2.length();
		index2 = data.indexOf(token_MAPS2END, index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		String lastt = data.substring(index1, index2);

		index1 = lastt.lastIndexOf(">");
		if (index1 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		index1 += ">".length();
		String lastpage = lastt.substring(index1);
		len = IntegerUtils.parseIntOrDefault(lastpage, -1);
		if (len < 0) {
			throw new CatalogException("Failed to read last page of map list.");
		}
		l.operationInProgress("Found " + len + " pages of maps data");

		index1 = lastt.lastIndexOf("start=");
		if (index1 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		index1 += "start=".length();
		index2 = lastt.indexOf("\"", index1);
		if (index1 == -1) {
			throw new CatalogException("Failed to parse map list.");
		}
		String lastpageindex = lastt.substring(index1, index2);
		last = IntegerUtils.parseIntOrDefault(lastpageindex, -1);
		if (last < 0) {
			throw new CatalogException("Failed to read last page of map list.");
		}
		int pages = len - first;
		int off = last / pages;
		for (int i = 0; i <= pages; i++) {
			int ci = i * off;
			l.operationInProgress("Parsing " + ci + "/" + last);
			getMapsPage("http://www.swbfgamers.com/index.php?action=downloads;cat=1;sortby=date;orderby=desc;start=" + ci, maps);
		}

		l.operationInProgress("Map list fetch in progress");
	}

	private static final String token_LIST1 = "<tr><td><a href=\"http://www.swbfgamers.com/index.php?action=downloads";
	private static final String token_LIST1END = "<tr>";

	private static final String token_LIST2 = "colspan";

	private void getMapsPage(String url, List<CatalogMap> m) throws CatalogException {
		retriever.setURL(url);
		retriever.commitCurrent();
		String resp = retriever.getResponse();
		int index1, index2;
		index1 = resp.indexOf(token_LIST1);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map data");
		}
		index1 += token_LIST1END.length();
		index2 = resp.indexOf(token_LIST2, index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to read map data");
		}
		String maps = resp.substring(index1, index2);
		maps = maps.replaceAll("<tr>", "");
		String[] mp = maps.split("</tr>");
		for (String s : mp) {
			try {
				if (s.startsWith("<td>")) {
					String code = "???";
					s = s.replaceAll("<td>", "");
					String[] b = s.split("</td>");
					long size = toBytes(b[4]);
					String[] q = b[0].split("\"");
					String urll = q[1];
					String name = q[2].substring(1);
					name = name.substring(0, name.length() - 4);
					String author = b[7].split(">")[1].split("<")[0];
					CatalogMap map = new CatalogMap(code, name, size, b[4], urll, author);
					m.add(map);
				}
			} catch (Exception e) {
				continue;
			}
		}
	}

	private class downloadDetails {
		private String n;
		private String url;

		private downloadDetails(String name, String url) {
			this.n = name;
			this.url = url;
		}

		protected String getURL() {
			return url;
		}

		protected String getFileName() {
			return n;
		}
	}

	private static final String token_DET = "<tr class=\"windowbg2\">";
	private static final String token_DET2 = "<a href=\"";

	private downloadDetails getMapFileName(CatalogMap m) throws CatalogException {
		retriever.setGet();
		retriever.setURL(m.getURL());
		retriever.commitCurrent();
		String resp = retriever.getResponse();
		int index1, index2;
		index1 = resp.indexOf(token_DET);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += token_DET.length();
		index1 = resp.indexOf(token_DET2, index1);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += token_DET2.length();
		index2 = resp.indexOf("\"", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		String url = resp.substring(index1, index2);
		index1 = resp.indexOf(">", index2);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += ">".length();
		index2 = resp.indexOf("<", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		String name = resp.substring(index1, index2);
		return new downloadDetails(name, url);
	}

	private static long toBytes(String filesize) {
		long returnValue = -1;
		Pattern patt = Pattern.compile("([\\d.]+)([GMK]B)", Pattern.CASE_INSENSITIVE);
		Matcher matcher = patt.matcher(filesize);
		Map<String, Integer> powerMap = new HashMap<String, Integer>();
		powerMap.put("GB", 3);
		powerMap.put("MB", 2);
		powerMap.put("KB", 1);
		if (matcher.find()) {
			String number = matcher.group(1);
			int pow = powerMap.get(matcher.group(2).toUpperCase());
			BigDecimal bytes = new BigDecimal(number);
			bytes = bytes.multiply(BigDecimal.valueOf(1024).pow(pow));
			returnValue = bytes.longValue();
		}
		return returnValue;
	}

	public static class CatalogException extends Exception {
		private static final long serialVersionUID = -7932540556992865376L;

		private CatalogException(String text) {
			super(text);
		}
	}
}
