package org.spigot.swbfserver.maps;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;


public class addmeScriptParser {

	public static Map[] getMap(File f) {
		int rein1 = 200;
		int rein2 = 200;
		String planet;
		String battle;
		try {
			byte[] data = Files.readAllBytes(f.toPath());
			String d = new String(data);
			int index = d.indexOf("mapluafile")+("mapluafile".length()+1);
			ByteBuffer b = ByteBuffer.wrap(data, index, data.length - index);
			b.order(ByteOrder.LITTLE_ENDIAN);
			getString(b);
			//String mapcode = getString(b);
			getString(b);
			String mapName = getString(b);
			index = d.indexOf("AddDownloadableContent")+"AddDownloadableContent".length()+1;
			b = ByteBuffer.wrap(data, index, data.length - index);
			b.order(ByteOrder.LITTLE_ENDIAN);
			String reinn1 = getString(b);
			String reinn2 = getString(b);
			String[] q = mapName.split(":", 2);
			if (q.length == 1) {
				planet = null;
				battle = mapName.trim();
			} else {
				planet = q[0].trim();
				battle = q[1].trim();
			}
			if (reinn2.isEmpty()) {
				return new Map[] { new Map(reinn1, rein1, rein2, planet, battle) };
			} else {
				return new Map[] { new Map(reinn1, rein1, rein2, planet, battle), new Map(reinn2, rein1, rein2, planet, battle) };
			}
		} catch (Exception e) {
			return null;
		}
	}

	private static String getString(ByteBuffer b) {
		int len=b.getInt();
		byte[] dst = new byte[len];
		b.get(dst);
		return new String(dst).trim();
	}
}
