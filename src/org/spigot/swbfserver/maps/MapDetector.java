package org.spigot.swbfserver.maps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.spigot.swbfserver.servers.ServerManager;

public class MapDetector {

	public static List<Map> getMaps() {
		List<Map> m = new ArrayList<>();
		getBuiltIn(m);
		getAddonFolder(m);
		return m;
	}

	private static final Comparator<Map> mapComp = new Comparator<Map>() {

		@Override
		public int compare(Map o1, Map o2) {
			return o1.getPureMapCode().compareTo(o2.getPureMapCode());
		}

	};

	public static List<Map> getEnabledMaps() {
		List<Map> m = new ArrayList<>();
		getAddonFolder(m);
		Collections.sort(m, mapComp);
		List<Map> mm = new ArrayList<>();
		String last = "";
		for (Map d : m) {
			String c = d.getPath();
			if (!last.equals(c)) {
				last = c;
				mm.add(d);
			}
		}
		return mm;
	}

	public static List<Map> getDisabledMaps() {
		List<Map> m = new ArrayList<>();
		getDisabledFolder(m);
		Collections.sort(m, mapComp);
		List<Map> mm = new ArrayList<>();
		String last = "";
		for (Map d : m) {
			String c = d.getPath();
			if (!last.equals(c)) {
				last = c;
				mm.add(d);
			}
		}
		return mm;
	}

	private static void processAddonFolder(List<Map> m, String path) {
		File f = new File(path);
		if (f.exists()) {
			File[] dirs = f.listFiles();
			for (File d : dirs) {
				File addme = new File(d.getAbsoluteFile() + "/addme.script");
				if (addme.exists()) {
					Map[] maps = addmeScriptParser.getMap(addme);
					for (Map mm : maps) {
						mm.setFolder(d.getAbsolutePath());
						m.add(mm);
					}
				}
			}
		} else {
			f.mkdirs();
		}
	}

	private static String getDisabledMapFolder() {
		return ServerManager.getServerFolderPath() + "/inactiveMaps";
	}

	private static String getEnabledMapFolder() {
		return ServerManager.getServerFolderPath() + "/AddOn";
	}

	private static void getDisabledFolder(List<Map> m) {
		String path = getDisabledMapFolder();
		processAddonFolder(m, path);
	}

	private static void getAddonFolder(List<Map> m) {
		String path = getEnabledMapFolder();
		processAddonFolder(m, path);
	}

	private static void getBuiltIn(List<Map> m) {
		m.add(new Map("bes1a", 200, 200));
		m.add(new Map("bes2a", 200, 200));
		m.add(new Map("end1a", 200, 200));
		m.add(new Map("hot1i", 250, 250));
		m.add(new Map("kas1i", 200, 200));
		m.add(new Map("kas2i", 250, 250));
		m.add(new Map("nab1i", 200, 200));
		m.add(new Map("nab2a", 200, 200));
		m.add(new Map("rhn1i", 250, 250));
		m.add(new Map("rhn2a", 200, 200));
		m.add(new Map("tat1i", 250, 250));
		m.add(new Map("tat2i", 200, 200));
		m.add(new Map("yav1i", 200, 200));
		m.add(new Map("yav2i", 200, 200));

		m.add(new Map("bes1r", 200, 200));
		m.add(new Map("bes2r", 200, 200));
		m.add(new Map("geo1r", 250, 250));
		m.add(new Map("kam1c", 200, 200));
		m.add(new Map("kas1c", 200, 200));
		m.add(new Map("kas2c", 250, 250));
		m.add(new Map("nab1c", 200, 200));
		m.add(new Map("nab2c", 200, 200));
		m.add(new Map("rhn1r", 250, 250));
		m.add(new Map("rhn2c", 200, 200));
		m.add(new Map("tat1r", 250, 250));
		m.add(new Map("tat2r", 200, 200));
		m.add(new Map("yav1c", 200, 200));
		m.add(new Map("yav2r", 200, 200));
	}

	public static void disableMap(Map m) {
		String p = m.getPath();
		if (p != null) {
			try {
				File f = new File(p);
				if (f.exists()) {
					FileUtils.moveDirectory(f, new File(getDisabledMapFolder() + "/" + f.getName()));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void enableMap(Map m) {
		String p = m.getPath();
		if (p != null) {
			try {
				File f = new File(p);
				if (f.exists()) {
					FileUtils.moveDirectory(f, new File(getEnabledMapFolder() + "/" + f.getName()));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getMapOrigin(Map map) {
		String path = map.getPath();
		if (path != null) {
			File f = new File(path + "/origin.url");
			if (f.exists()) {
				try {
					return new String(Files.readAllBytes(f.toPath()));
				} catch (IOException e) {
				}
			}
		}
		return null;
	}

	public static void setMapOrigin(String targetfile, String url) {
		File f = new File(targetfile + "/origin.url");
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				return;
			}
		}
		FileOutputStream s;
		try {
			s = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			return;
		}
		try {
			s.write(url.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
