package org.spigot.swbfserver.utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

public class SuperTable extends JPanel {

	private static final long serialVersionUID = -1352925739995750155L;
	private JComponent[] header;
	private MigLayout layout;
	private int columnsLen;

	public SuperTable(Object[] Columns, String[] lengths) {
		columnsLen = Columns.length;
		StringBuilder sb = new StringBuilder("");
		header = new JComponent[columnsLen];
		for (int i = 0; i < columnsLen; i++) {
			sb.append("[" + lengths[i] + "]");
			header[i] = new JPanel();
			header[i].add(Columns[i] instanceof JComponent ? (JComponent) Columns[i] : new JLabel(Columns[i].toString()));
			header[i].setBackground(new Color(0xFFDDCC));
		}
		layout = new MigLayout("", sb.toString(), "[]");
		super.setLayout(layout);
		addHeader();
	}

	private void addHeader() {
		for (int i = 0; i < columnsLen; i++) {
			super.add(header[i], "cell " + i + " 0, growx");
		}
	}

	public void setData(Object[][] data) {
		super.removeAll();
		super.setLayout(layout);
		int len = data.length;
		StringBuilder sb = new StringBuilder("[]");
		for (int i = 0; i < len; i++) {
			sb.append("[]");
		}
		layout.setRowConstraints(sb.toString());
		addHeader();
		for (int i = 0; i < len; i++) {
			Object[] q = data[i];
			for (int o = 0; o < q.length; o++) {
				Object d = q[o];
				JComponent n;
				if (d instanceof JComponent) {
					n = (JComponent) d;
				} else {
					n = new JLabel(d.toString());
				}
				JPanel p = new JPanel();
				p.setLayout(new BorderLayout());
				p.add(n, BorderLayout.CENTER);
				p.setBackground(i % 2 == 0 ? new Color(0xCCEEFF) : new Color(0x99EEFF));
				super.add(p, "cell " + o + " " + (i + 1) + ", grow");
			}
		}
	}

	public void enableComponents(boolean enable) {
		enableComponents(this, enable);
	}

	private void enableComponents(Container container, boolean enable) {
		Component[] components = container.getComponents();
		for (Component component : components) {
			component.setEnabled(enable);
			if (component instanceof Container) {
				enableComponents((Container) component, enable);
			}
		}
	}

	public void clear() {
		super.removeAll();
		super.setLayout(layout);
		layout.setRowConstraints("[]");
		addHeader();
	}

	public void replace(final JComponent b, final JComponent a) {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					Container parent = b.getParent();
					parent.remove(b);
					parent.add(a, BorderLayout.CENTER);
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
		}
	}
}
