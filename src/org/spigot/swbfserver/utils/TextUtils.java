package org.spigot.swbfserver.utils;


public class TextUtils {
	/*
	 * public static String join(String glue, List<Object> data) { StringBuilder sb = new StringBuilder(); for (Object d : data) { sb.append(d + glue); } if (sb.length() > 0) { sb.setLength(sb.length() - 1); } return sb.toString(); }
	 */

	/**
	* Returns list of multiple {@link CharSequence} joined into a single
	* {@link CharSequence} separated by localized delimiter such as ", ".
	*
	* @hide
	*/
	public static CharSequence join(Iterable<CharSequence> list) {
		final CharSequence delimiter = ",";
		return join(delimiter, list);
	}

	/**
	 * Returns a string containing the tokens joined by delimiters.
	 * @param tokens an array objects to be joined. Strings will be formed from
	 *     the objects by calling object.toString().
	 */
	public static String join(CharSequence delimiter, Object[] tokens) {
		StringBuilder sb = new StringBuilder();
		boolean firstTime = true;
		for (Object token : tokens) {
			if (firstTime) {
				firstTime = false;
			} else {
				sb.append(delimiter);
			}
			sb.append(token);
		}
		return sb.toString();
	}

	/**
	 * Returns a string containing the tokens joined by delimiters.
	 * @param tokens an array objects to be joined. Strings will be formed from
	 *     the objects by calling object.toString().
	 */
	public static String join(CharSequence delimiter, Iterable<?> tokens) {
		StringBuilder sb = new StringBuilder();
		boolean firstTime = true;
		for (Object token : tokens) {
			if (firstTime) {
				firstTime = false;
			} else {
				sb.append(delimiter);
			}
			sb.append(token);
		}
		return sb.toString();
	}

}
