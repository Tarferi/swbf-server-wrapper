package org.spigot.swbfserver.utils;

import java.io.InputStream;

import org.spigot.swbfserver.utils.JHTTPClient.Method;

public class URLRetriever {
	private boolean isClosed = false;
	static final String COOKIES_HEADER = "Set-Cookie";

	private JHTTPClient client;

	public void addPostField(String key, String value) {
		client.addPostField(key, value);
	}

	public void setPost() {
		client.setMethod(Method.POST);
	}

	public void setGet() {
		client.setMethod(Method.GET);
	}

	public void setURL(String url) {
		client.setURL(url);
	}

	public String getLastPageTitle() {
		return client.getLastResponseTitle();
	}

	public void addGETData(String key, String value) {
		client.addGetField(key, value);
	}

	public void commitCurrent() {
		if (isClosed) {
			System.err.println("Failed to execute retrieve operation on closed client!");
			return;
		}
		client.commitCurrent();
	}

	public InputStream commitCurrentGetInput() {
		return commitCurrentGetInput(null);
	}

	public InputStream commitCurrentGetInput(MaxLengthAvailableListener l) {
		if (isClosed) {
			System.err.println("Failed to execute retrieve operation on closed client!");
			return null;
		}
		client.commitCurrent();
		if (l != null) {
			if (client.hasKnownLength()) {
				l.maxLength(client.getLastLength());
			}
		}
		return client.getLastResponseStream();
	}

	public boolean hasResponse() {
		return client.getLastResponse() != null;
	}

	public String getResponse() {
		return client.getLastResponse();
	}

	public URLRetriever() {
		client = new JHTTPClient();
	}
/*
	public final File commitToFile(ProgressListener p) {
		try {
			File tmp = MAIN.getTempFile();
			FileOutputStream out = new FileOutputStream(tmp);
			IOUtils.copy(commitCurrentGetInput(), out, p);
			return tmp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
*/
	public static interface MaxLengthAvailableListener {
		public void maxLength(int length);
	}

	public static final String getURLContent(String url, MaxLengthAvailableListener l) {
		try {
			JHTTPClient c = new JHTTPClient();
			c.setURL(url);
			c.commitCurrent();
			if (l != null) {
				if (c.hasKnownLength()) {
					l.maxLength(c.getLastLength());
				}
			}
			return c.getLastResponse();
		} catch (Exception e) {
			return null;
		}
	}

	public static final String getURLContent(String url) {
		return getURLContent(url, null);
	}

	public String getCurrentURL() {
		return client.getURL();
	}

	public void close() {
		isClosed = true;
		client.close();
	}
}
