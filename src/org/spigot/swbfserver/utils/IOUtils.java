package org.spigot.swbfserver.utils;

import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

	public static class CopyOperation {
		private boolean stop = false;
		private ProgressListener listener;
		private OutputStream out;
		private InputStream in;

		private CopyOperation(InputStream in, OutputStream out, ProgressListener listener) {
			this.in=in;
			this.out=out;
			this.listener=listener;
		}

		public void stop() {
			stop = true;
		}

		public void commit() {
			try {
			byte[] buffer = new byte[bufferLength];
			long count = 0;
			int n = 0;
			while (EOF != (n = in.read(buffer))) {
				if (stop) {
					listener.stopped();
					break;
				}
				out.write(buffer, 0, n);
				listener.setValue(count);
				count += n;
			}
			} catch (Throwable e) {
				listener.fail(e);
			}
		}
	}

	public static interface ProgressListener {
		public void setValue(long bytes);

		public void setMaximum(long maximum);
		
		public void stopped();
		
		public void fail(Throwable e);
	}

	private static final int bufferLength = 1024 * 4;
	private static final int EOF = -1;

	public static final CopyOperation copy(InputStream in, OutputStream out) {
		return copy(in, out, null);
	}

	public static final CopyOperation copy(InputStream in, OutputStream out, ProgressListener listener) {
		return new CopyOperation(in, out, listener);
	}

	public static void closeQuietly(Closeable out) {
		try {
			out.close();
		} catch (Throwable e) {
		}
	}
}
