package org.spigot.swbfserver.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpCookie;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;

public class JHTTPClient {

	private Socket sock;
	private String url;
	private String lastResponse;
	private boolean lastRead;
	private InputStream input;
	private CookieHandler cookies = new CookieHandler();
	private final Map<String, String> post = new HashMap<>();
	private final Map<String, String> get = new HashMap<>();

	private boolean chunked = false;

	private Method method = Method.GET;
	private String lastPageTitle;
	private int lastLength;
	private Encoding enc = Encoding.NONE;
	private int lastResponseCode;
	private int redirectCount;
	private int maxRedirects = 10;

	public int getMaxRedirectCount() {
		return maxRedirects;
	}

	public void setMaxRedirectCount(int c) {
		maxRedirects = c;
	}

	public int getLastResponseCode() {
		return lastResponseCode;
	}

	public boolean hasKnownLength() {
		return lastLength != -1;
	}

	public int getLastLength() {
		return lastLength;
	}

	private enum Encoding {
		NONE, GZIP;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method m) {
		this.method = m;
	}

	public void clearCookies() {
		cookies.clear();
	}

	private static final String agent = "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";// "JHTTPClient";
	private static final String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
	private static final String ACCEPTENC = "gzip, deflate, sdch";
	private static final String ACCEPTLANG = "cs,en;q=0.8,sk;q=0.6";
	private static final String CONTENTTYPE = "application/x-www-form-urlencoded";

	public JHTTPClient() {
	}

	public static enum Method {
		POST("POST"), GET("GET");
		private String s;

		private Method(String s) {
			this.s = s;
		}

		@Override
		public String toString() {
			return s;
		}
	}

	public void setURL(String url) {
		this.url = url;
	}

	public String getURL() {
		return url;
	}

	private static final String getHost(String s) {
		if (s.startsWith("http://")) {
			s = s.substring("http://".length());
		}
		int i = s.indexOf("/");
		return s.substring(0, i == -1 ? s.length() : i);

	}

	@SuppressWarnings("deprecation")
	private static final String getURLEncodedString(String s) {
		return URLEncoder.encode(s);
	}

	private static final String Implode(Map<String, String> m) {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, String> p : m.entrySet()) {
			sb.append(getURLEncodedString(p.getKey()) + "=" + getURLEncodedString(p.getValue()) + "&");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	private static final String getEndpoint(String s, Map<String, String> get) {
		if (!s.startsWith("http")) {
			s = "http://" + s;
		}
		if (get.isEmpty()) {
			return s;
		} else {
			String getData = Implode(get);
			if (s.contains("?")) {
				s = s + "&" + getData;
			} else {
				s = s + "?" + getData;
			}
			return s;
		}
	}

	private class CookieHandler {

		private Map<String, HttpCookie> cookies = new HashMap<>();

		public void addCookies(Iterable<HttpCookie> cs) {
			for (HttpCookie c : cs) {
				cookies.put(c.getName(), c);
			}
		}

		public void clear() {
			cookies.clear();
		}

		public void refreshCookies() {
			Map<String, HttpCookie> k = new HashMap<>();
			for (Entry<String, HttpCookie> c : cookies.entrySet()) {
				if (!c.getValue().hasExpired()) {
					k.put(c.getKey(), c.getValue());
				}
			}
			cookies = k;
		}

		public Iterable<HttpCookie> getCookies() {
			return cookies.values();
		}

		public boolean isEmpty() {
			return cookies.isEmpty();
		}
	}

	public void commitCurrent() {
		redirectCount = 0;
		commitCurrentRaw();
	}

	@Override
	public void finalize() {
		close();
	}

	private void commitCurrentRaw() {
		redirectCount++;
		if (redirectCount > maxRedirects) {
			lastResponse = null;
			return;
		}
		StringBuilder header = new StringBuilder();
		header.append(method.toString() + " " + getEndpoint(url, get) + " HTTP/1.1\r\n");
		header.append("User-Agent: " + agent + "\r\n");
		header.append("Host: " + getHost(url) + "\r\n");
		header.append("Accept: " + ACCEPT + "\r\n");
		header.append("Content-Type: " + CONTENTTYPE + "\r\n");
		header.append("Accept-Encoding: " + ACCEPTENC + "\r\n");
		header.append("Accept-Language: " + ACCEPTLANG + "\r\n");
		header.append("Connection: close\r\n");

		if (!cookies.isEmpty()) {
			header.append("Cookie: ");
			for (HttpCookie cookie : cookies.getCookies()) {
				header.append(cookie.toString() + "; ");
			}
			header.setLength(header.length() - 2);
			header.append("\r\n");
		}
		if (!post.isEmpty()) {
			String postData = Implode(post);
			header.append("Content-Length: " + postData.length() + "\r\n");
			header.append("\r\n");
			header.append(postData);
		} else {
			header.append("\r\n");
		}
		InputStream purein = pureInput(commitData(getHost(url), header.toString()));
		get.clear();
		post.clear();
		enc = Encoding.NONE;
		lastRead = false;
		chunked = false;
		String[] ss = getHeaderLine(purein).split(" ", 3);
		String code = ss[1];
		try {
			lastResponseCode = Integer.parseInt(code);
		} catch (Exception e) {
			lastResponseCode = 0;
		}
		lastLength = -1;
		method = Method.GET;
		List<String> headers = new ArrayList<>();
		while (true) {
			String s = getHeaderLine(purein);
			headers.add(s + "\n");
			if (s.isEmpty()) {
				break;
			}
			String[] r = s.split(": ", 2);
			if (r.length != 2) {
				break;
			}
			String key = r[0].trim().toLowerCase();
			String value = r[1].trim();
			switch (key) {
				case "location":
					if (value.startsWith("http")) {
						url = value;
					} else {
						url = getHost(url) + "/" + value;
					}
					cookies.refreshCookies();
					close();
					commitCurrentRaw();
					return;
				case "set-cookie":
				case "set-cookie2":
					if (!value.isEmpty()) {
						cookies.addCookies(HttpCookie.parse(s));
					}
				break;
				case "content-length":
					try {
						lastLength = Integer.parseInt(value);
					} catch (Exception e) {
						lastLength = 0;
					}
				break;
				case "transfer-encoding":
					chunked = value.equalsIgnoreCase("chunked");
				break;
				case "content-encoding":
					if (value.equalsIgnoreCase("gzip")) {
						enc = Encoding.GZIP;
					}
				break;
			}
		}
		cookies.refreshCookies();
		if (chunked) {
			input = chunkedinput(purein);
		} else {
			input = purein;
		}
		if (enc == Encoding.GZIP) {
			input = gzipInput(input);
		}
	}

	public final InputStream pureInput(final InputStream input) {
		return new InputStream() {

			private final InputStream in = input;
			private boolean finished = false;

			@Override
			public int read() throws IOException {
				if (finished) {
					return -1;
				} else {
					int i;
					try {
						i = in.read();
					} catch (Throwable e) {
						e.printStackTrace();
						i = -1;
					}
					if (i == -1) {
						in.close();
						finished = true;
					}
					return i;
				}
			}

			@Override
			public String toString() {
				return "HTTPInputStream " + super.toString();
			}

		};
	}

	public void copyStream(OutputStream out) {

	}

	private InputStream gzipInput(InputStream input2) {
		try {
			return new GZIPInputStream(input2);
		} catch (IOException e) {
			return new InputStream() {

				@Override
				public int read() throws IOException {
					return -1;
				}

				@Override
				public String toString() {
					return "GzipInputStream " + super.toString();
				}
			};
		}
	}

	private InputStream chunkedinput(final InputStream purein) {
		return new InputStream() {
			private final InputStream in = purein;

			int lastLen = 0;
			private boolean finished = false;

			private void updateLastLen() {
				String line = getHeaderLine(in).trim();
				try {
					lastLen = Integer.parseInt(line, 16);
				} catch (Exception e) {
					finished = true;
					lastLen = 0;
				}
			}

			@Override
			public String toString() {
				return "ChunkedInputStream " + super.toString();
			}

			@Override
			public int read() throws IOException {
				if (finished) {
					return -1;
				} else {
					if (lastLen == 0) {
						updateLastLen();
						if (lastLen == 0) {
							finished = true;
							return -1;
						}
						return read();
					} else {
						lastLen--;
						int i = purein.read();
						if (lastLen == 0) {
							purein.read();
							purein.read();
							return i;
						} else {
							return i;
						}
					}
				}
			}

		};
	}

	private static final String getHeaderLine(InputStream input) {
		StringBuilder sb = new StringBuilder();
		while (true) {
			try {
				int i = input.read();
				if (i == -1) {
					break;
				} else {
					char c = (char) i;
					if (c == '\n') {
						break;
					} else {
						if (c != '\r') {
							sb.append(c);
						}
					}
				}
			} catch (Throwable e) {
				break;
			}
		}
		return sb.toString();
	}

	public void close() {
		try {
			sock.close();
		} catch (Throwable e) {
		}
	}

	private final InputStream commitData(String host, String data) {
		try {
			sock = new Socket(host, 80);
			sock.getOutputStream().write(data.getBytes());
			return sock.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public String getLastResponse() {
		if (!lastRead) {
			lastRead = true;
			readLast();
		}
		return lastResponse;
	}

	private void readLast() {
		try {
			if (lastLength == -1) {
				StringWriter writer = new StringWriter();
				IOUtils.copy(input, writer);
				lastResponse = writer.toString();
			} else {
				lastResponse = getLastResponse(input, lastLength);
			}
			close();
		} catch (IOException e) {
			e.printStackTrace();
			lastResponse = null;
		}
	}

	private static final String getLastResponse(InputStream in, int length) throws IOException {
		if (length == 0) {
			return "";
		} else {
			int read = 0;
			byte[] buffer = new byte[length];
			while (read != length) {
				read += in.read(buffer, read, length - read);
				if (read < 0) {
					return "";
				}
			}
			return new String(buffer);
		}
	}

	public InputStream getLastResponseStream() {
		lastRead = true;
		lastResponse = "";
		return input;
	}

	public void addPostField(String key, String value) {
		post.put(key, value);
	}

	public void addGetField(String key, String value) {
		get.put(key, value);
	}

	public String getLastResponseTitle() {
		String response = getLastResponse();
		if (response != null) {
			int p1 = response.indexOf("<title>") + "<title>".length();
			int p2 = response.indexOf("</title>", p1);
			try {
				lastPageTitle = response.substring(p1, p2).trim();
			} catch (Exception e) {
				lastPageTitle = null;
			}
		} else {
			lastPageTitle = null;
		}
		return lastPageTitle;
	}

}
