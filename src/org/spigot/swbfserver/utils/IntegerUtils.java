package org.spigot.swbfserver.utils;

public class IntegerUtils {

	public static int parseIntOrDefault(String s, int def) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return def;
		}
	}
}
